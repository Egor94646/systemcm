import json


def reading_mqtt_camera():
    """
    чтение mqtt параметров камеры
    :return: словарь параметров камеры
    """
    with open('./configuration/mqttSettingsCamera.json', 'r', encoding='utf-8') as json_file:
        dict_json = json.load(json_file)
    return dict_json


def reading_json(path_json):
    """
    чтение json файлов
    :param path_json: путь к json файлу
    :return: словрь
    """
    with open(path_json, 'r', encoding='utf-8') as json_file:
        dict_json = json.load(json_file)
    return dict_json


def save_json(path_json, data):
    """
    сохраняет словрь data по пути path_json
    :param path_json: путь к json файлу
    :param data: сохраняемый словарь
    """
    with open(path_json, 'w', encoding='utf-8') as json_file:
        json.dump(data, json_file, indent=2)
