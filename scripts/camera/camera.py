import time
from PyQt5 import uic
from paho.mqtt import client as mqtt_client
from PyQt5.QtWidgets import QWidget, QSlider, QGridLayout
from PyQt5.QtGui import QPixmap
import serial
from serial.threaded import ReaderThread, Protocol
from scripts.scripts_common import ReadJson
import serial.tools.list_ports as list_ports
from PyQt5.QtWidgets import QWidget, QLabel, QApplication, QButtonGroup, QLayout
from PyQt5 import QtWidgets, uic
import sys
import PyQt5
from queue import Queue


class camera(QWidget):
    def __init__(self):
        super(camera, self).__init__()
        self.ui = uic.loadUi('ui/camera.ui')
        self.mqtt_ui = uic.loadUi('ui/mqtt_settings.ui')
        self.object_ui = uic.loadUi('ui/object.ui')
        self.pole_ui = uic.loadUi('ui/pole.ui')
        self.init_ui()

    def init_ui(self):
        self.ui.pushButton_mqtt.clicked.connect(self.mqtt_ui.show)
        self.ui.pushButton_pole.clicked.connect(self.pole_ui.show)

    def read_mqtt_json(self):
        self.mqttConfig = ReadJson.reading_json("mqtt_settings.json")
        self.set_wiget(name_layout='formLayout_7', ui=self.mqtt_ui, name_func='filling_mqtt_widgets')

    def save_mqtt(self):
        self.mqttConfig = {
            "broker": self.mqtt_ui.broker.text(),
            "port": self.mqtt_ui.port.text(),
            "topic": self.mqtt_ui.topic.text(),
            "client_id": self.mqtt_ui.client_id.text(),
            "username": self.mqtt_ui.username.text(),
            "password": self.mqtt_ui.password.text()
        }
        ReadJson.save_json(path_json='mqtt_settings.json', data=self.mqttConfig)
        self.ui.textBrowser.append('Успешно сохраненно\r\n')

    def set_wiget(self, name_layout: str, name_func=None, value=False, ui=None):
        """
        метод для работы с ui
        перебирает все элементы в определенно слое интерефейса и производит с ними опредленные действия
        :param name_layout: имя слоя в котором будем перебирать все элементы
        :param name_func: имя функции, которая будет обрабатывать объекты, если её нет, то будет измененно состояние объектов
        :param value: в случае отсутсвия метода для обработки объектов измененно будет состояние на данное значение
        """
        q = Queue()
        if ui is None:
            q.put(getattr(self.ui, name_layout))
        else:
            q.put(getattr(ui, name_layout))
        while q.qsize() != 0:
            l = q.get()
            for j in range(l.count()):
                obj = l.itemAt(j)
                if type(obj) != PyQt5.QtWidgets.QWidgetItem:
                    q.put(obj)
                else:
                    if name_func is not None:
                        func = getattr(self, name_func)
                        func(obj)
                    else:
                        obj.widget().setEnabled(value)

    def register_handler(self, event: str, func_arr: list):
        """
        метод добавления нового события, или обновления старого
        :param event: название события
        :param func_arr: список имен функций
        """
        event_handlers = ReadJson.reading_json('settings/handlers_event.json')
        functions = event_handlers.get(event)
        if functions is None:
            event_handlers[event] = [func.__name__ for func in func_arr]
        else:
            functions.append(*[func.__name__ for func in func_arr])
        ReadJson.save_json('handlers_json.json', event_handlers)

    def dispatch(self, event: str, data_list: list):
        """
        инициализация ивента, перебор и запуск всех методов в ивенте
        :param event: имя ивента
        :param data: информация получаемая ивентом
        """
        event_handlers = ReadJson.reading_json('handlers_json.json')
        functions = event_handlers.get(event)
        if functions is None:
            raise ValueError(f'Unknow event {event}')
        for func, data in zip(functions, data_list):
            func_get = getattr(self, func)
            if data is not None:
                func_get(data)
            else:
                func_get()


def launch():
    app = QtWidgets.QApplication(sys.argv)
    gui = camera()
    gui.ui.show()
    sys.exit(app.exec_())


launch()