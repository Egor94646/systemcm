import time
from PyQt5 import uic
from paho.mqtt import client as mqtt_client
from PyQt5.QtWidgets import QWidget, QSlider, QGridLayout
from PyQt5.QtGui import QPixmap
import serial
from serial.threaded import ReaderThread, Protocol
from scripts.scripts_common import ReadJson
import serial.tools.list_ports as list_ports
from PyQt5.QtWidgets import QWidget, QLabel, QApplication, QButtonGroup, QLayout
from PyQt5 import QtWidgets, uic
import sys
import PyQt5
from queue import Queue
from delta_cinematic import Delta_ck

#класс обработки потока сериал порта
class SerialReaderProtocolRaw(Protocol):

    def connection_made(self, transport):
        """
        метод события запуска потока
        """
        print("Connected, ready to receive data...")
        gui.ui_report('IEconnect')

    def data_received(self, data):
        """
        Событие получение данных с сериал порта
        :param data: информация полученная с сериал порта
        """
        gui.ui_report('DE'+data.decode('utf-8'))


class Device(QWidget):
    # region Init
    def __init__(self):
        super(Device, self).__init__()
        self.ui = uic.loadUi('Device.ui')
        self.mqtt_ui = uic.loadUi('mqtt_settings.ui')
        self.device_ui = uic.loadUi('device_settings.ui')
        self.ser = None
        self.client = None
        self.list_layout = ['Console', 'Mqtt', 'Slider']
        self.init_ui_device()
        self.init_ui_mqtt_settings()
        self.dispatch(event='start', data_list=[None, None])
        self.device_parameters = {'type': self.device_ui.Select_Manip.currentText(),'port': self.device_ui.Port.currentText(), 'baudrate': self.device_ui.comboBox.currentText()}

        self.slider_settings = ReadJson.reading_json('settings_slider.json')

        self.delta_coord = {'x': 0, 'y': 0, 'z': 0, 'a': 0, 'b': 0, 'f': 30}
        self.delta_small = Delta_ck(e=80.0, f=140.0, re=365.0, rf=80.0, l=100.0)
        self.delta_big = Delta_ck(e=105.0, f=123.0, re=445.0, rf=95.0, l=0.0)
    def status_sensors(self):
        #сделать по человечески
        error = QPixmap('icons/error.jpg')
        self.ui.COMStat.setPixmap(error)
        self.ui.COMStat.setToolTip("error")
        self.ui.ManipStat.setPixmap(error)
        self.ui.ManipStat.setToolTip("error")
        self.ui.MQTTStat.setPixmap(error)
        self.ui.MQTTStat.setToolTip("error")

    def init_ui_device(self):
        """
        привзка событий api клиента к методам
        """
        self.group_console_button = QButtonGroup()
        self.group_radio_button = QButtonGroup()

        self.set_wiget('verticalLayout', name_func='add_radiodutton')
        self.set_wiget('M_Buttons', name_func='add_button')
        self.set_wiget('G_Buttons', name_func='add_button')

        self.group_radio_button.buttonClicked.connect(self.click_radio_button)
        self.group_console_button.buttonClicked.connect(self.click_console_button)

        self.device_ui.buttonBox.accepted.connect(self.device_button_ok)
        self.device_ui.buttonBox.rejected.connect(self.device_button_cancel)

        self.ui.pushButton.clicked.connect(lambda: self.dispatch("open_device_setting_api", [None, None]))
        self.ui.Console_input.editingFinished.connect(self.send_console)

        for i in ['x', 'y', 'z', 'a', 'b', 'f']:
            self.set_wiget(f'verticalLayout_{i}', name_func='connect_slider')
        self.ui.send_slider.clicked.connect(self.send_slider)

        self.ui.runButton.clicked.connect(lambda: self.dispatch(event='open_mqtt_settings_api', data_list=[None, None]))
        self.ui.stopPushButton.clicked.connect(lambda: self.dispatch(event="disconnection_mqtt", data_list=[None, True]))



    def init_ui_mqtt_settings(self):
        self.mqtt_ui.connectionPushButton.clicked.connect(lambda: self.dispatch(event="connection_mqtt", data_list=[None, None, None, False]))
        self.mqtt_ui.saveMqttPushButton.clicked.connect(self.save_mqtt)

    def device_show(self):
        self.device_ui.show()

    def mqtt_show(self):
        self.mqtt_ui.show()

    def read_mqtt_json(self):
        self.mqttConfig = ReadJson.reading_json("mqtt_settings.json")
        self.set_wiget(name_layout='formLayout_7', ui=self.mqtt_ui, name_func='filling_mqtt_widgets')

    def curtain(self):
        """
        Метод принуждающий пользователя выбрать тип манипулятора
        :return:
        """
        self.ui_report('IEselect the type of manipulator')

        self.ui.pushButton.setStyleSheet("""
                        QWidget {
                            background-color: rgb(255, 255, 51);
                            }
                        """)
        for i in self.list_layout:
            self.set_wiget(i)
            eval(f'self.ui.{i}_indicator.setEnabled(False)')

    # endregion

    # region Mqtt
    def save_mqtt(self):
        self.mqttConfig = {
            "broker": self.mqtt_ui.broker.text(),
            "port": self.mqtt_ui.port.text(),
            "topic": self.mqtt_ui.topic.text(),
            "client_id": self.mqtt_ui.client_id.text(),
            "username": self.mqtt_ui.username.text(),
            "password": self.mqtt_ui.password.text()
        }
        ReadJson.save_json(path_json='mqtt_settings.json', data=self.mqttConfig)
        self.ui_report('IEsuccessfully saved')

    def filling_mqtt_widgets(self, obj):
        if obj.widget().objectName() in list(self.mqttConfig.keys()):
            obj.widget().setText(self.mqttConfig[obj.widget().objectName()])

    def disconnect_mqtt(self):
        if self.client is not None:
            self.client.disconnect()
            self.client = None
            self.ui_report('MTdisconnect')
        else:
            self.ui_report('MTyou are not connected to mqtt')

    def connect_mqtt(self):

        def on_connect(client, userdata, flags, rc):
            """
            метод события подключения к mqtt брокеру
            :param rc: состояния подключения к mqtt серверу
            """
            if rc == 0:
                self.ui_report('MTconnected to MQTT Broker!')
                return 0
            else:
                self.ui_report("MTfailed to connect, return code %d ", rc)
                return rc

        try:
            self.client = mqtt_client.Client(self.mqttConfig["client_id"])
            self.client.username_pw_set(self.mqttConfig["username"], self.mqttConfig["password"])
            self.client.on_connect = on_connect
            self.client.connect(self.mqttConfig["broker"], int(self.mqttConfig["port"]))
            self.client.loop_start()
        except ValueError:
            self.ui_report('MTinvalid port value', 'ValueError')
        except Exception:
            self.ui_report('MTinvalid broker', 'socket.gaierror')

    def subscribe(self):
        """
        метод подписки на топик
        """
        try:
            def on_message(client, userdata, msg):
                """
                метод вызываемый при получении сообщения в подключенный топик
                :param msg: полученное сообщения
                """
                self.ui_report(f"MTreceived `{msg.payload.decode()}` from `{msg.topic}` topic")
                coord = msg.payload.decode()
                self.dispatch_in_serial_port(coord)

            self.client.subscribe(self.mqttConfig["topic"])
            self.client.on_message = on_message
        except Exception:
            pass
        else:
            self.ui_report(f"MTsuccessful subscription to topic {self.mqttConfig['topic']}")

    def mqtt_close(self):
        self.mqtt_ui.close()

    def changing_state_unwanted_elements(self, state):
        self.ui.Console_indicator.setEnabled(state)
        self.ui.Slider_indicator.setEnabled(state)
        self.ui.runButton.setEnabled(state)
    # endregion


    # region serial_port
    def update_com_port(self):
        """
        метод обновлеия списка портов доступных для подключения устройств
        """
        self.device_ui.Port.clear()
        list_com = list_ports.comports()
        list_item_port = [''] if self.ser is None else ['', 'Port Close']
        for i in list_com:
            if i.pid is not None:
                list_item_port.append(i.device)
        self.device_ui.Port.addItems(list_item_port)

    def dispatch_in_serial_port(self, str):
        """
        отправляет в serial port g-code команду
        :param str: g-code команда для отправки в serial port
        """
        if self.ser is not None:
            m = str
            m += '\n'
            self.ui_report('DEsend to the serial port: \n' + m)
            b = bytes(m, encoding='utf-8')
            self.ser.write(b)
        else:
            self.ui_report('DEport not selected')
            self.dispatch(event="error", data_list=[1])


    def serial_change(self,  port):
        self.ser = serial.Serial(port)
        self.ser.baudrate = self.device_parameters['baudrate']

    def serial_close(self, ser):
        self.ser.close()
        self.ser = None

    def send_console(self):
        text = self.ui.Console_input.text()
        try:
            if 'g0' in text and self.device_parameters['type'][0:5] == 'delta':
                text += ' '
                coord = []
                for i in ['x', 'y', 'z', 'a', 'b', 'f']:
                    if i in text:
                        coord.append(
                            int(text[text.find(i) + 1: text.find(' ', text.find(i))]))
                        self.delta_coord[i] = text[text.find(i) + 1: text.find(' ', text.find(i))]
                    else:
                        coord.append(self.delta_coord[i])
                f_coord = eval(f'self.{self.device_parameters["type"]}.inverse_cinematic(coord[0], coord[1], coord[2])')
                if f_coord != -1:
                    self.dispatch_in_serial_port(f'g16 U{f_coord[0]} V{f_coord[1]} W{f_coord[2]} A{coord[3]} B{coord[4]} F{coord[5]}')
                    self.ui_report(f'DEg16 U{f_coord[0]} V{f_coord[1]} W{f_coord[2]} A{coord[3]} B{coord[4]} F{coord[5]}')
                else:
                    self.ui_report('IEincorrect coordinates')
        except:
            self.ui_report('IEincorrect coordinates')
        else:
            self.dispatch_in_serial_port(text)

    def send_slider(self):
        match self.device_parameters['type']:
            case 'Scara':
                self.dispatch_in_serial_port(f'g0 x{self.ui.Slider_x.value()} '
                                             f'y{self.ui.Slider_y.value()} '
                                             f'z{self.ui.Slider_z.value()} '
                                             f'a{self.ui.Slider_a.value()}')
            case 'delta_small'|'delta_big':
                i = eval(f'self.{self.device_parameters["type"]}.inverse_cinematic(self.ui.Slider_x.value(), self.ui.Slider_y.value(), self.ui.Slider_z.value())')
                if i == -1:
                    self.ui_report('IEincorrect coordinates', 'error')
                else:
                    self.dispatch_in_serial_port(f'g16 U{i[0]} V{i[1]} W{i[2]} A{self.ui.Slider_a.value()} F{self.ui.Slider_f.value()}')

    def connected_text_browser_device(self, work: bool):
        reader = ReaderThread(self.ser, SerialReaderProtocolRaw)
        reader.start()

    def device_button_ok(self):
        self.ui_report('IEthe parameters are selected')
        self.device_parameters = {
            'type': str(self.device_ui.Select_Manip.currentText()),
            'port': str(self.device_ui.Port.currentText()),
            'baudrate': str(self.device_ui.comboBox.currentText())}
        match self.device_parameters['type']:
            case 'Select manipulator':
                self.curtain()
            case _:
                self.ui.pushButton.setStyleSheet("""QWidget {}""")
                for i in self.list_layout:
                    eval(f'self.ui.{i}_indicator.setEnabled(True)')
                self.ui.Console_indicator.setChecked(True)
                self.click_radio_button(self.ui.Console_indicator)
                for p in ['x', 'y', 'z', 'a', 'b', 'f']:
                    eval(f'self.ui.Slider_{p}.setMinimum({str(self.slider_settings.get(self.device_parameters["type"]).get(p)[0])})')
                    eval(f'self.ui.Slider_{p}.setMaximum({str(self.slider_settings.get(self.device_parameters["type"]).get(p)[1])})')

        match self.device_parameters['port']:
            case 'Port Close':
                self.dispatch('port_close', [None, 0, False])
            case '':
                pass
            case _:
                self.dispatch('port_change',
                              [self.device_parameters['port'], 0, True])

    def device_button_cancel(self):
        self.device_ui.Select_Manip.setCurrentText(
            self.device_parameters['type'])
        self.device_ui.Port.setCurrentText(self.device_parameters['port'])
        self.device_ui.comboBox.setCurrentText(
            self.device_parameters['baudrate'])
        self.ui_report('IEthe parameters remained the same')

    # endregion

    # region ui
    def ui_report(self, text:str, error=None):
        """
        IE, DE, MT - Идексы перед сообщение, определяют в какой браузер пойдёт сообщение.(InterfacE, DevicE, MqtT соответственно)
        :param text: сообщение, тип str
        :param error: Ошибка если есть. Если отсутствует, то сообщение будет определенно как уведомление
        :return:
        """

        current_time = time.strftime("%H:%M:%S", time.localtime())
        if text[0:2] == 'DE':
            self.ui.errorTextBrowser.append(f'{current_time} | {error if error is not None else "notify"} | {text[2:]}')
        elif text[0:2] == 'MT':
            self.ui.errorTextBrowser_2.append(f'{current_time} | {error if error is not None else "notify"} | {text[2:]}')
        elif text[0:2] == 'IE':
            self.ui.textBrowser.append(f'{current_time} | {error if error is not None else "notify"} | {text[2:]}')

    def ui_indicator(self, id):
        match id:
            case 0:
                ok = QPixmap('icons/OK.jpg')
                self.ui.COMStat.setPixmap(ok)
                self.ui.COMStat.setToolTip("OK")
            case 1:
                error = QPixmap('icons/Error.jpg')
                self.ui.COMStat.setPixmap(error)
                self.ui.COMStat.setToolTip("COM not opened")


    def set_wiget(self, name_layout: str, name_func=None, value=False, ui=None):
        """
        метод для работы с ui
        перебирает все элементы в определенно слое интерефейса и производит с ними опредленные действия
        :param name_layout: имя слоя в котором будем перебирать все элементы
        :param name_func: имя функции, которая будет обрабатывать объекты, если её нет, то будет измененно состояние объектов
        :param value: в случае отсутсвия метода для обработки объектов измененно будет состояние на данное значение
        """
        q = Queue()
        if ui is None:
            q.put(getattr(self.ui, name_layout))
        else:
            q.put(getattr(ui, name_layout))
        while q.qsize() != 0:
            l = q.get()
            for j in range(l.count()):
                obj = l.itemAt(j)
                if type(obj) != PyQt5.QtWidgets.QWidgetItem:
                    q.put(obj)
                else:
                    if name_func is not None:
                        func = getattr(self, name_func)
                        func(obj)
                    else:
                        obj.widget().setEnabled(value)

    def add_radiodutton(self, obj):
        if type(obj.widget()) == PyQt5.QtWidgets.QRadioButton:
            self.group_radio_button.addButton(obj.widget())

    def add_button(self, obj):
        if type(obj.widget()) == PyQt5.QtWidgets.QPushButton:
            self.group_console_button.addButton(obj.widget())

    def click_radio_button(self, button):
        """
        включает группу виджетов работы с устройством, в зависимости от выбранного
        :param button: кнопка на которую нажал пользователь
        """
        self.selected_layer = button.text()
        list_layout = self.list_layout.copy()
        del list_layout[list_layout.index(self.selected_layer)]
        self.set_wiget(self.selected_layer, value=True)
        self.set_wiget(list_layout[0])
        self.set_wiget(list_layout[1])

    def click_console_button(self, button):
        self.dispatch_in_serial_port(button.text())

    def connect_slider(self, obj):
        """
        метод подключащий событие изменения слайдеро и устанавливающий дизайн дисплея
        :param obj: объект api
        """
        for i in ['x', 'y', 'z', 'a', 'b', 'f']:
            if obj.widget() == getattr(self.ui, f'Slider_{i}'):
                label = getattr(self.ui, f'lcdNumber_{i}')
                label.setStyleSheet("""
                        QWidget {
                            border: 20px solid black;
                            border-radius: 10px;
                            background-color: rgb(255, 255, 255);
                            }
                        """)
                obj.widget().valueChanged.connect(label.display)
    # endregion

    # region events
    def register_handler(self, event: str, func_arr: list):
        """
        метод добавления нового события, или обновления старого
        :param event: название события
        :param func_arr: список имен функций
        """
        event_handlers = ReadJson.reading_json('handlers_json.json')
        functions = event_handlers.get(event)
        if functions is None:
            event_handlers[event] = [func.__name__ for func in func_arr]
        else:
            functions.append(*[func.__name__ for func in func_arr])
        ReadJson.save_json('handlers_json.json', event_handlers)

    def dispatch(self, event: str, data_list: list):
        """
        инициализация ивента, перебор и запуск всех методов в ивенте
        :param event: имя ивента
        :param data: информация получаемая ивентом
        """
        event_handlers = ReadJson.reading_json('handlers_json.json')
        functions = event_handlers.get(event)
        if functions is None:
            raise ValueError(f'Unknow event {event}')
        for func, data in zip(functions, data_list):
            func_get = getattr(self, func)
            if data is not None:
                func_get(data)
            else:
                func_get()

    # endregion


def launch():
    global gui
    app = QtWidgets.QApplication(sys.argv)
    gui = Device()
    gui.ui.show()
    sys.exit(app.exec_())


launch()
