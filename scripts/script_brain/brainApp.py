from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QWidget, QLabel, QApplication
from PyQt5 import QtWidgets, uic
import sys
from scripts.scripts_common import ReadJson
import threading
from scripts.script_brain import mqttBrain
import math


class BrainApp(QWidget):
    def __init__(self):
        super(BrainApp, self).__init__()

        self.ui = uic.loadUi('././ui/Brain_gui.ui')

        self.mqttCamera = ReadJson.reading_json('././configuration/mqttSettingsCamera.json')
        self.mqttBrain = ReadJson.reading_json('././configuration/mqttSettingsBrain.json')
        self.mqttManipulator = ReadJson.reading_json('././configuration/mqttSettingsManipulator.json')

        self.mqttWork = mqttBrain.MqttBrain(self.ui)

        self.init_mqtt_show()
        self.init_mqtt_edit()
        self.init_buttons()
        self.angel = 0

    # region Mqtt
    def init_buttons(self):
        self.ui.mqttSavePushButton.clicked.connect(self.save_mqtt)
        self.ui.connectMqtt.clicked.connect(self.connaect_mqtt)
        self.ui.disconnectMqtt.clicked.connect()

    def init_mqtt_show(self):
        self.ui.brokerLineEdit.setText(str(self.mqttBrain["settings"]["broker"]))
        self.ui.passwordLineEdit.setText(str(self.mqttBrain["settings"]["password"]))
        self.ui.clientIdLineEdit.setText(str(self.mqttBrain["settings"]["username"]))
        self.ui.portLineEdit.setText(str(self.mqttBrain["settings"]["port"]))

        self.ui.topicCoordLineEdit.setText(str(self.mqttBrain["settings"]["topic"]["coordinate_mm"]))
        self.ui.topicStateLineEdit.setText(str(self.mqttBrain["settings"]["topic"]["state"]))

        self.ui.usrCameraLineEdit.setText(str(self.mqttCamera["settings"]["client_id"]))
        self.ui.usrManipulatorLineEdit.setText(str(self.mqttManipulator["settings"]["client_id"]))
        self.ui.usrBrainLineEdit.setText(str(self.mqttBrain["settings"]["client_id"]))

    def init_mqtt_edit(self):
        self.ui.brokerLineEdit.editingFinished.connect(lambda: self.edit_broker())
        self.ui.passwordLineEdit.editingFinished.connect(lambda: self.edit_password())
        self.ui.clientIdLineEdit.editingFinished.connect(lambda: self.edit_cliend_id())
        self.ui.portLineEdit.editingFinished.connect(lambda: self.edit_port())

        self.ui.topicCoordLineEdit.editingFinished.connect(lambda: self.edit_topic_coord())
        self.ui.topicStateLineEdit.editingFinished.connect(lambda: self.edit_topic_state())

        self.ui.usrCameraLineEdit.editingFinished.connect(lambda: self.edit_usr_camera())
        self.ui.usrManipulatorLineEdit.editingFinished.connect(lambda: self.edit_usr_manipulator())
        self.ui.usrBrainLineEdit.editingFinished.connect(lambda: self.edit_usr_brain())

    # region edit mqtt
    def edit_broker(self):
        broker = self.ui.brokerLineEdit.text()
        self.mqttBrain["settings"]["broker"] = broker

    def edit_password(self):
        password = self.ui.passwordLineEdit.text()
        self.mqttBrain["settings"]["password"] = password

    def edit_client_id(self):
        client_i = self.ui.clientIdLineEdit.text()
        self.mqttBrain["settings"]["username"] = client_i

    def edit_port(self):
        port = self.ui.portLineEdit.text()
        self.mqttBrain["settings"]["port"] = port

    def edit_topic_coord(self):
        topic_coord = self.ui.topicCoordLineEdit.text()
        self.mqttBrain["settings"]["topic"]["coordinate"] = topic_coord

    def edit_topic_state(self):
        topic_state = self.ui.topicStateLineEdit.text()
        self.mqttBrain["settings"]["topic"]["state"] = topic_state

    def edit_usr_camera(self):
        usr_camera = self.ui.usrCameraLineEdit.text()
        self.mqttCamera["settings"]["client_id"] = usr_camera

    def edit_usr_manipulator(self):
        usr_manipulator = self.ui.usrManipulatorLineEdit.text()
        self.mqttManipulator["settings"]["client_id"] = usr_manipulator

    def edit_usr_brain(self):
        usr_brain = self.ui.usrBrainLineEdit.text()
        self.mqttBrain["settings"]["client_id"] = usr_brain
    # endregion

    def save_mqtt(self):
        ReadJson.save_json("././configuration/mqttSettingsCamera.json", self.mqttCamera)
        ReadJson.save_json("././configuration/mqttSettingsBrain.json", self.mqttBrain)
        ReadJson.save_json("././configuration/mqttSettingsCamera.json", self.mqttManipulator)

    def init_obj_mqtt(self):
        self.mqttWork.connect_mqtt()
        self.mqttWork.subscribe()

    def publish_mqtt(self):
        a1, a2 = self.reverse_kinematics()
        self.mqttWork.publish(str(a1) + str(a2) + str(self.angel))

    def subscriber_mqtt(self):
        self.msg = self.mqttWork.coord_mm
        if self.msg == '':
            self.ui.showMqttLable.setText('None')
        else:
            self.ui.showMqttLable.setText(self.msg)
            self.msg = self.msg.split(' ')
            self.xmm = int(self.msg[0])
            self.ymm = int(self.msg[1])
            self.angel = int(self.msg[2])
    # endregion

    def reverse_kinematics_scara(self, x, y):
        d = (math.sqrt(x ** 2 + y ** 2))
        PI = math.pi
        l1 = 150
        l2 = 240
        if x >= 0:
            q1 = math.asin(y / d)
            q2 = math.acos((d ** 2 + l1 ** 2 - l2 ** 2) / (2 * d * l1))
            q3 = PI - math.acos((l1 ** 2 + l2 ** 2 - d ** 2) / (2 * l1 * l2))
            a1 = PI - (q1 + q2)
            a2 = (q3 + PI / 2)
            alpha1 = a1 * (180 / PI)
            alpha2 = a2 * (180 / PI)
            print(f'{alpha1}, {alpha2}')
        if x < 0:
            q1 = math.asin(y / d)
            q2 = math.acos((d ** 2 + l1 ** 2 - l2 ** 2) / (2 * d * l1))
            q3 = math.acos((l1 ** 2 + l2 ** 2 - d ** 2) / (2 * l1 * l2))
            a1 = q1 + q2
            a2 = PI - ((PI - q3) + PI / 2)
            alpha1 = a1 * (180 / PI)
            alpha2 = a2 * (180 / PI)
            print(f'{alpha1}, {alpha2}')

        return alpha1, alpha2
