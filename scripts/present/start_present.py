import numpy as np
from PyQt5 import QtCore, QtWidgets
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from PIL import Image, ImageChops
from scipy import ndimage
import math
import cv2
import sys
from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import QWidget, QLabel, QApplication, QDialog
from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage, QPixmap
import time
import serial
import image_test


class Start_present(QThread, QWidget):
    changePixmap = pyqtSignal(QImage)

    def __init__(self):
        super().__init__()
        self.ui = uic.loadUi('Present.ui')
        self.threadactive = True

        self.order_marks = [4, 5, 1, 6, 7]
        self.wmm = 770
        self.hmm = 380

        self.dictionary = cv2.aruco.Dictionary_get(cv2.aruco.DICT_6X6_250)
        self.parameters = cv2.aruco.DetectorParameters_create()

        self.ui.pushButton.clicked.connect(self.start_button)
        self.ui.pushButton_2.clicked.connect(self.init_ser)

        self.pt = []
        self.x_mm = 1
        self.y_mm = 1
        self.cam = cv2.VideoCapture(0)
        self.flag = False

    def run(self):

        while True:
            ret, self.src = self.cam.read()

            if ret:
                self.markerCorners, self.markerIds, self.rejectedCandidates = cv2.aruco.detectMarkers(self.src,
                                                                                                      self.dictionary,
                                                                                                      parameters=self.parameters)
                if self.markerIds is not None:
                    if len(self.markerIds) >= 4:
                        self.id = []
                        for i in self.markerIds:
                            self.id.append(i[0])
                        result = all(x in self.id for x in self.order_marks)
                        if result:
                            self.pt_angels()
                            self.max_width()
                            self.max_height()
                            self.math_out()

                            self.pt.clear()

    def stop(self):
        self.threadactive = False
        self.wait()

    def restart(self):
        self.cam = 0
        self.threadactive = True
        self.start()

    def pt_angels(self):
        for i in self.order_marks:
            center = [int((self.markerCorners[self.id.index(i)][0][0][0] + self.markerCorners[self.id.index(i)][0][2][0]) / 2),
             int((self.markerCorners[self.id.index(i)][0][0][1] + self.markerCorners[self.id.index(i)][0][2][1]) / 2)]
            self.pt.append(center)

    def max_width(self):
        width_AD = np.sqrt(((self.pt[0][0] - self.pt[3][0]) ** 2) + ((self.pt[0][1] - self.pt[3][1]) ** 2))
        width_BC = np.sqrt(((self.pt[1][0] - self.pt[2][0]) ** 2) + ((self.pt[1][1] - self.pt[2][1]) ** 2))
        self.MaxWidth = max(int(width_AD), int(width_BC))

    def max_height(self):
        height_AB = np.sqrt(((self.pt[0][0] - self.pt[1][0]) ** 2) + ((self.pt[0][1] - self.pt[1][1]) ** 2))
        height_CD = np.sqrt(((self.pt[2][0] - self.pt[3][0]) ** 2) + ((self.pt[2][1] - self.pt[3][1]) ** 2))
        self.maxHeight = max(int(height_AB), int(height_CD))
    # endregion

    def math_out(self):
        input_pts = np.float32([self.pt[0], self.pt[1], self.pt[2], self.pt[3]])

        output_pts = np.float32([[0, 0],
                                 [0, self.maxHeight - 1],
                                 [self.MaxWidth - 1, self.maxHeight - 1],
                                 [self.MaxWidth - 1, 0]])

        self.M = cv2.getPerspectiveTransform(input_pts, output_pts)
        self.out = cv2.warpPerspective(self.src, self.M, (self.MaxWidth, self.maxHeight), flags=cv2.INTER_LINEAR)

    def math_cube(self, thresh):
        def warPerspective_point(x, y):
            m = [
                [x],
                [y],
                [1]
            ]
            m3 = [[m[0][0] * self.M[0][0] + m[1][0] * self.M[0][1] + m[2][0] * self.M[0][2]],
                  [m[0][0] * self.M[1][0] + m[1][0] * self.M[1][1] + m[2][0] * self.M[1][2]],
                  [m[0][0] * self.M[2][0] + m[1][0] * self.M[2][1] + m[2][0] * self.M[2][2]]]
            return m3



        contours0, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_TREE,
                                                cv2.CHAIN_APPROX_SIMPLE)
        count=0
        if contours0 != ():
            for cnt in contours0:
                count += 1
                rect = cv2.minAreaRect(cnt)
                box = cv2.boxPoints(rect)
                center = [int(rect[0][0]), int(rect[0][1])]

                center_check = warPerspective_point(center[0], center[1])
                center_check = [int(center_check[0][0]), int(center_check[1][0])]
                if center_check[0] < 0 or center_check[1] < 0:
                    continue

                box = np.int0(box)
                area = int(rect[1][0] * rect[1][1])

                edge1 = np.int0((box[1][0] - box[0][0], box[1][1] - box[0][1]))
                edge2 = np.int0((box[2][0] - box[1][0], box[2][1] - box[1][1]))

                usedEdge = edge1
                if cv2.norm(edge2) > cv2.norm(edge1):
                    usedEdge = edge2
                self.reference = (1, 0)
                self.angle = 180.0 / math.pi * math.acos(
                    (self.reference[0] * usedEdge[0] + self.reference[1] * usedEdge[1]) / (
                            cv2.norm(self.reference) * cv2.norm(usedEdge)))

                if area >= 100:
                    self.x_calibr = self.wmm / self.out.shape[1]
                    self.y_calibr = self.hmm / self.out.shape[0]

                    center_manipulator = [int((self.markerCorners[self.id.index(7)][0][0][0] +
                                   self.markerCorners[self.id.index(7)][0][2][0]) / 2),
                              int((self.markerCorners[self.id.index(7)][0][0][1] +
                                   self.markerCorners[self.id.index(7)][0][2][1]) / 2)]

                    m_manimpulator = warPerspective_point(center_manipulator[0], center_manipulator[1])
                    center_manipulator = [int(m_manimpulator[0][0]), int(m_manimpulator[1][0])]

                    m_obj = warPerspective_point(center[0], center[1])
                    center = [int(m_obj[0][0]), int(m_obj[1][0])]

                    self.x_mm = int((center_manipulator[
                        0] - center[0]) * self.x_calibr)
                    self.y_mm = int((center_manipulator[1] - center[
                        1]) * self.y_calibr + 170)

            print(count)


    def angel(self, x, y):
        d = (math.sqrt(x ** 2 + y ** 2))
        PI = math.pi
        l1 = 150
        l2 = 240
        if x >= 0:
            q1 = math.asin(y / d)
            q2 = math.acos((d ** 2 + l1 ** 2 - l2 ** 2) / (2 * d * l1))
            q3 = PI - math.acos((l1 ** 2 + l2 ** 2 - d ** 2) / (2 * l1 * l2))
            a1 = PI - (q1 + q2)
            a2 = (q3 + PI / 2)
            alpha1 = a1 * (180 / PI)
            alpha2 = a2 * (180 / PI)
        if x < 0:
            q1 = math.asin(y / d)
            q2 = math.acos((d ** 2 + l1 ** 2 - l2 ** 2) / (2 * d * l1))
            q3 = math.acos((l1 ** 2 + l2 ** 2 - d ** 2) / (2 * l1 * l2))
            a1 = q1 + q2
            a2 = PI - ((PI - q3) + PI / 2)
            alpha1 = a1 * (180 / PI)
            alpha2 = a2 * (180 / PI)

        return alpha1, alpha2

    def otpravlenie_in_serial_port(self, str):
        m = str
        m += '\n'
        b = bytes(m, encoding='utf-8')
        self.ser.write(b)

    def init_ser(self):
        self.ser = serial.Serial('COM7')
        self.ser.baudrate = 9600
        time.sleep(1)
        self.otpravlenie_in_serial_port('g28')
        time.sleep(10)
        self.otpravlenie_in_serial_port('g30')
        time.sleep(5)
        ret, self.start_img = self.cam.read()
        cv2.imwrite('start.jpg', self.start_img)

    def start_button(self):
        if not self.flag:
            self.flag = True
            if self.y_mm > 0:
                self.detect()
            else:
                print("error")
        else:
            if self.y_mm > 0:
                self.detect()
            else:
                print("error")

    def detect(self):
        start_img = cv2.imread('start.jpg')
        self.diff = image_test.diff_image(start_img, self.src)
        self.math_cube(self.diff)
        print(self.x_mm, self.y_mm)
        self.a1, self.a2 = self.angel(self.x_mm, self.y_mm)
        self.otpravlenie_in_serial_port(f'G16 O{self.a1} S{self.a2} A{self.angle}')
        #self.otpravlenie_in_serial_port(f'g0 x{self.x_mm} y{self.y_mm}')
        time.sleep(7)
        self.otpravlenie_in_serial_port('g0 z-100')
        time.sleep(5)
        self.otpravlenie_in_serial_port('m3')
        time.sleep(5)
        self.otpravlenie_in_serial_port('m3')
        time.sleep(5)
        self.otpravlenie_in_serial_port('g0 z-10')
        time.sleep(7)
        self.otpravlenie_in_serial_port('g0 x-200 y0')
        time.sleep(8)
        self.otpravlenie_in_serial_port('m4')
        time.sleep(7)
        self.otpravlenie_in_serial_port('m4')
        time.sleep(3)
        self.otpravlenie_in_serial_port('g30')
        time.sleep(7)


def launch():
    app = QtWidgets.QApplication(sys.argv)
    gui = Start_present()
    gui.start()
    gui.ui.show()
    sys.exit(app.exec_())

launch()

