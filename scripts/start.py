from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QWidget, QLabel, QApplication
from PyQt5 import QtWidgets, uic
import sys
from scripts.scripts_camera import cameraApp
from scripts.script_manipulator import manipulatorApp
from scripts.script_brain import brainApp


class Start(QWidget):
    def __init__(self):
        super(Start, self).__init__()

        self.start_ui = uic.loadUi('./ui/start_gui.ui')

        self.init_ui()

    def init_ui(self):
        self.start_ui.startBrainPushButton.clicked.connect(self.brain_window)
        self.start_ui.startCameraPushButton.clicked.connect(self.camera_window)
        self.start_ui.startManipulatorPushButton.clicked.connect(self.manipulator_window)
        self.start_ui.exitPushButton.clicked.connect(self.start_ui.close)

    def camera_window(self):
        """
        старт окна камеры и закрытие стартового окна
        """
        self.cameraWindow = cameraApp.CameraApp()
        self.cameraWindow.camera_ui.show()

    def manipulator_window(self):
        """
        старт окна манипулятора и закрытие стартового окна
        """
        self.manipulatowindow = manipulatorApp.MainGui()
        self.manipulatowindow.ui.show()

    def brain_window(self):
        self.brainwindow = brainApp.BrainApp()
        self.brainwindow.ui.show()


def launch():
    app = QtWidgets.QApplication(sys.argv)
    gui = Start()
    gui.start_ui.show()
    sys.exit(app.exec_())
