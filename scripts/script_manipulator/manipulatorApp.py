import time
from PyQt5 import uic
from PyQt5.QtWidgets import QWidget, QSlider, QGridLayout
from PyQt5.QtGui import QPixmap
import serial
from PyQt5.uic.properties import QtGui
from scripts.scripts_common import ReadJson
from scripts.script_manipulator import MqttSubscribeManipulator
from threading import Timer
import serial.tools.list_ports as list_ports
import threading
import math


class MainGui(QWidget):
    def __init__(self):
        super().__init__()

        self.ok = QPixmap("././ui/icons/OK.jpg")
        self.warn = QPixmap("././ui/icons/Warning.jpg")
        self.err = QPixmap("././ui/icons/Error.jpg")

        self.ui = uic.loadUi('././ui/Manipulator_gui.ui')

        self.config_mqtt = ReadJson.reading_json('././configuration/mqttSettingsManipulator.json')

        self.list_com = list_ports.comports()
        self.com_opened = False
        self.ports = []

        self.ui.Select_Manip.activated[str].connect(self.text_changed)
        self.selected_manip = False

        self.connect_mqtt = False

        self.init_button()
        self.init_slider()
        self.init_mqtt()
        self.init_com()
        self.init_statusbar()

        #self.type_work()

        self.ymm = '100'
        self.xmm = '100'

        self.msg = ''

        self.radioButton = -1

        self.page = self.ui.Tabs.widget(1)
        if self.page is not None:
            self.page.setEnabled(False)

        self.check = True

    def enabled_slider(self, value):
        qgrid = self.ui.findChildren(QGridLayout)[0]
        for i in range(0, qgrid.count()):
            obj = qgrid.takeAt(i)
            if obj is not None:
                obj.widget().setEnabled(value)

    @staticmethod
    def thread_metod(fn):
        def run(*k, **kw):
            t = threading.Thread(target=fn, args=k, kwargs=kw)
            t.start()
            return t
        return run
    # region Init

    def init_button(self):
        """
        подключение всех кнопок
        """
        self.ui.Butt_G28.clicked.connect(self.g28_clicked)
        self.ui.Butt_G30.clicked.connect(self.g30_clicked)

        self.ui.Butt_M1.clicked.connect(self.m1_clicked)
        self.ui.Butt_M2.clicked.connect(self.m2_clicked)
        self.ui.Butt_M3.clicked.connect(self.m3_clicked)
        self.ui.Butt_M4.clicked.connect(self.m4_clicked)

        self.ui.Butt_Send.clicked.connect(self.console_send)
        self.ui.send_slider.clicked.connect(self.slider_send)

        self.ui.Console_indicator.toggled.connect(self.console)
        self.ui.Slider_indicator.toggled.connect(self.slider)
        self.ui.mqttRadioButton.toggled.connect(self.mqtt)

        self.ui.Port.currentTextChanged.connect(self.port_change)

        self.ui.Butt_Send.clicked.connect(self.send_console)

        self.ui.startPushButton.clicked.connect(self.start_button)

    def init_slider(self):
        """
        подключение к слайдерам
        """
        self.ui.Slider_1.valueChanged.connect(self.sl1_value_changed)
        self.ui.Console_Slide_1.editingFinished.connect(self.sl1_edfinish)

        self.ui.Slider_2.valueChanged.connect(self.sl2_value_changed)
        self.ui.Console_slide_2.editingFinished.connect(self.sl2_edfinish)

        self.ui.Slider_3.valueChanged.connect(self.sl3_value_changed)
        self.ui.Console_slide_3.editingFinished.connect(self.sl3_edfinish)

        self.ui.Slider_4.valueChanged.connect(self.sl4_value_changed)
        self.ui.Console_slide_4.editingFinished.connect(self.sl4_edfinish)

        self.ui.Slider_5.valueChanged.connect(self.sl5_value_changed)
        self.ui.Console_slide_5.editingFinished.connect(self.sl5_edfinish)

    def radio_button(self):
        self.ui.Console_indicator.toggled.connect(self.onClicked)
        self.ui.Slider_indicator.toggled.connect(self.onClicked)
        self.ui.mqttRadioButton.toggled.conneect(self.onClicked)

    def text_changed(self, text):
        """
        подключаемый метод к выпадаещему списку манипуляторов
        :param text: получает текущую позицию списка
        """
        match text:
            case 'Scara':
                self.page.setEnabled(True)
                self.ui.Slider_1.setMinimum(-380)
                self.ui.Slider_1.setMaximum(380)
                self.ui.Slider_2.setMaximum(380)
                self.ui.Slider_2.setMinimum(0)
                self.ui.Slider_3.setMaximum(0)
                self.ui.Slider_3.setMinimum(-100)
                self.ui.Slider_4.setMaximum(180)
                self.ui.Slider_5.setMaximum(100)

                self.ui.Butt_M1.setEnabled(False)
                self.ui.Butt_M2.setEnabled(False)
                self.ui.Slider_5.setEnabled(False)
                self.ui.Slide_5_label.setEnabled(False)
                self.ui.Console_slide_5.setEnabled(False)
                self.selected_manip = True
                self.ui.ManipStat.setPixmap(self.ok)
                self.ui.ManipStat.setToolTip("OK")

            case 'Select manipulator':
                self.page = self.ui.Tabs.widget(1)
                if self.page is not None:
                    self.page.setEnabled(False)
                self.selected_manip = False
                self.ui.ManipStat.setPixmap(self.err)
                self.ui.ManipStat.setToolTip("Manipulator not selected")

    def init_mqtt(self):
        """
        подключение кнопок и компонентов для mqtt
        """
        self.ui.saveMqttPushButton.clicked.connect(self.save_mqtt)
        self.ui.connectionPushButton.clicked.connect(self.connect)
        self.ui.checkPushButton.clicked.connect(self.check_mqtt)
        self.ui.disconnectPushButton.clicked.connect(self.disconnect_mqtt)
        self.ui.runButton.clicked.connect(self.run_mqtt)
        self.ui.stopPushButton.clicked.connect(self.stop_mqtt)

        self.mqtt_connect_edit()
        self.mqtt_show_edit()

    def init_statusbar(self):
        """
        Инициализация строки состояния
        """
        self.ui.COMStat.setPixmap(self.err)
        self.ui.ManipStat.setPixmap(self.err)
        self.ui.ManipStat.setToolTip("Manipulatror not selected")
        self.ui.MQTTStat.setPixmap(self.err)

    def init_com(self):
        """
        Метод добавляющий порты в QCombobox Port
        """
        for i in self.list_com:
            self.ports.append(i.device)
        self.ui.Port.addItems(self.ports)

    # endregion

    # region Control_tab

    # region WriteSerial
    def otpravlenie_in_serial_port(self, str):
        m = str
        m += '\n'
        b = bytes(m, encoding='utf-8')
        self.ser.write(b)

    @thread_metod
    def output_serial_port(self):
        while True:
            self.text_serial = self.ser.readline().decode('utf-8')
            self.ui.errorTextBrowser.append(self.text_serial)
            print(self.text_serial)

    @thread_metod
    def write_serialport(self, array):
        """
        отправляет в Serial порт манипулятора строку str
        :param str: отправляемая строка
        """
        self.check = False
        while array:
            self.otpravlenie_in_serial_port(array[0])
            while self.text_serial != 'OK!\r\n':
                pass
            time.sleep(8)
            del array[0]
        self.check = True

    def g28_clicked(self):
        """
        метод подключаемый к кнопке g28, отправляющий в Serial потр `g28`
        """
        #self.write_serialport(['g28'])
        self.subscriber.publish('g28', 'K/RX')

    def g30_clicked(self):
        self.subscriber.publish('g30', 'HC/RX')

    def m1_clicked(self):
        self.write_serialport(['m1'])

    def m2_clicked(self):
        self.write_serialport(['m2'])

    def m3_clicked(self):
        self.write_serialport(['m3'])

    def m4_clicked(self):
        self.write_serialport(['m4'])

    def send_console(self):
        text = self.ui.Console.text()
        self.write_serialport([text])

    def start_button(self):
        self.write_serialport([f'g0 x{self.xmm} y{self.ymm} z-10 a{self.angel}', 'g0 z-80', 'm3', 'm3', 'g0 z0', 'g30', 'm4'])

    # endregion

    # region sliders
    def sl1_value_changed(self, value):
        """
        выводим в окна вывода координату - value
        :param value: координата
        """
        self.ui.Console_Slide_1.setText(str(value))
        self.ui.Slide_1_label.setText(str(value))
        self.c = math.sqrt(value ** 2 + self.ui.Slider_2.value() ** 2)
        if self.c >= 185 and self.c <= 380:
            self.ui.send_slider.setStyleSheet('')
            self.ui.send_slider.setEnabled(True)
        else:
            self.ui.send_slider.setStyleSheet('background: rgb(255,0,0);')
            self.ui.send_slider.setEnabled(False)

    def sl1_edfinish(self):
        """
        метод подключаемый к слайдеру sl1
        """
        self.ui.Slider_1.setValue(int(self.ui.Console_Slide_1.text()))
        self.ui.Slide_1_label.setText(str(self.ui.Slider_1.value()))
        self.ui.Console_Slide_1.setText(str(self.ui.Slider_1.value()))

    def sl2_value_changed(self, value):
        self.ui.Console_slide_2.setText(str(value))
        self.ui.Slide_2_label.setText(str(value))
        self.c = math.sqrt(self.ui.Slider_1.value() ** 2 + value ** 2)
        if self.c >= 185 and self.c <= 380:
            self.ui.send_slider.setStyleSheet('')
            self.ui.send_slider.setEnabled(True)
        else:
            self.ui.send_slider.setStyleSheet('background: rgb(255,0,0);')
            self.ui.send_slider.setEnabled(False)

    def sl2_edfinish(self):
        self.ui.Slider_2.setValue(int(self.ui.Console_slide_2.text()))
        self.ui.Slide_2_label.setText(str(self.ui.Slider_2.value()))
        self.ui.Console_slide_2.setText(str(self.ui.Slider_2.value()))

    def sl3_value_changed(self, value):
        self.ui.Console_slide_3.setText(str(value))
        self.ui.Slide_3_label.setText(str(value))

    def sl3_edfinish(self):
        self.ui.Slider_3.setValue(int(self.ui.Console_slide_3.text()))
        self.ui.Slide_3_label.setText(str(self.ui.Slider_3.value()))
        self.ui.Console_slide_3.setText(str(self.ui.Slider_3.value()))

    def sl4_value_changed(self, value):
        self.ui.Console_slide_4.setText(str(value))
        self.ui.Slide_4_label.setText(str(value))

    def sl4_edfinish(self):
        self.ui.Slider_4.setValue(int(self.ui.Console_slide_4.text()))
        self.ui.Slide_4_label.setText(str(self.ui.Slider_4.value()))
        self.ui.Console_slide_4.setText(str(self.ui.Slider_4.value()))

    def sl5_value_changed(self, value):
        self.ui.Console_slide_5.setText(str(value))
        self.ui.Slide_5_label.setText(str(value))

    def sl5_edfinish(self):
        self.ui.Slider_5.setValue(int(self.ui.Console_slide_5.text()))
        self.ui.Slide_5_label.setText(str(self.ui.Slider_5.value()))
        self.ui.Console_slide_5.setText(str(self.ui.Slider_5.value()))

    def port_change(self, text):
        """
        Метод меняющий текущий Com порт
        """
        match text:
            case 'Port Close':
                self.ser.close()
                self.com_opened = False
                self.ui.COMStatLbl.setText("COM:")
                self.ui.COMStat.setPixmap(self.warn)
                self.ui.COMStat.setToolTip("COM not opened")

            case _:
                self.ser = serial.Serial(text)
                self.ser.baudrate = 9600
                self.com_opened = True
                self.ui.COMStatLbl.setText("COM" + self.ui.Port.currentText()[3:4] + ":")
                self.output_serial_port()
                self.ui.COMStat.setPixmap(self.ok)
                self.ui.COMStat.setToolTip("OK")
    # endregion
    def console_send(self):
        """
        метод подключаемый к кнопке `run`
        в зависимости от выбранной радиокнопки отправляет разные координаты
        -1 - ничего не выбрано
        0 - команда введена вручную
        """
        match self.radioButton:
            case - 1:
                self.ui.errorTextBrowser.append('не выбран способ отправки координат\r\n')

            case 0:
                self.write_serialport([str(self.ui.Console.text())])
                print(self.ui.Console.text())

    def slider_send(self):
        """
        метод подключаемый к кнопке `run`
        в зависимости от выбранной радиокнопки отправляет разные координаты
        -1 - ничего не выбрано
        1 - координаты со слайдера
        """
        match self.radioButton:
            case -1:
                self.ui.errorTextBrowser.append('не выбран способ отправки координат\r\n')
            case 1:
                self.ui.Console.setText(
                    f'g0 x{self.ui.Slider_1.value()} y{self.ui.Slider_2.value()} z{self.ui.Slider_3.value()} a{self.ui.Slider_4.value()}')
                self.write_serialport([
                    f'G0 X{self.ui.Slider_1.value()} Y{self.ui.Slider_2.value()} Z{self.ui.Slider_3.value()} A{self.ui.Slider_4.value()}'])


    def console(self):
        """
        метод подключаемый к радиокнопке, устанавливает значение обрабатываеммое в методе run
        """
        self.radioButton = 0
        self.enabled_console(True)

    def slider(self):
        self.radioButton = 1
        self.enabled_slider(False)

    def mqtt(self):
        self.radioButton = 2

    # endregion

    # region Settings Mqtt
    def mqtt_connect_edit(self):
        """
        подключение считывания с LineEdit mqtt(настройки mqtt)
        """
        self.ui.brokerLineEdit.returnPressed.connect(lambda: self.edit_broker())
        self.ui.portLineEdit.returnPressed.connect(lambda: self.edit_port())
        self.ui.topicCoordLineEdit.returnPressed.connect(lambda: self.edit_topic())
        self.ui.clientIdLineEdit.returnPressed.connect(lambda: self.edit_client_id())
        self.ui.usernameLineEdit.returnPressed.connect(lambda: self.edit_username())
        self.ui.passwordLineEdit.returnPressed.connect(lambda: self.edit_password())

    def mqtt_show_edit(self):
        """
        вывод значений настроек mqtt из json
        """
        self.ui.brokerLineEdit.setText(str(self.config_mqtt["settings"]["broker"]))
        self.ui.portLineEdit.setText(str(self.config_mqtt["settings"]["port"]))
        self.ui.topicCoordLineEdit.setText(str(self.config_mqtt["settings"]["topic"]["coordinate_angel"]))
        self.ui.topicStateLineEdit.setText(str(self.config_mqtt["settings"]["topic"]["state"]))

        self.ui.clientIdLineEdit.setText(str(self.config_mqtt["settings"]["client_id"]))
        self.ui.usernameLineEdit.setText(str(self.config_mqtt["settings"]["username"]))
        self.ui.passwordLineEdit.setText(str(self.config_mqtt["settings"]["password"]))

    def connect(self):
        """
        метод подключения к mqtt
        """
        self.subscriber = MqttSubscribeManipulator.MqttSubscribeManipulator(self.ui)
        self.subscriber.subscribe("user_9dcd4eb4/coordinates_mm")
        self.connect_mqtt = True
        self.ui.MQTTStat.setPixmap(self.ok)
        self.ui.MQTTStat.setToolTip("OK")

    def disconnect_mqtt(self):
        """
        отключение от mqtt
        """
        if not self.connect_mqtt:
            self.ui.errorTextBrowser.append('нет подключения к mqtt брокеру\r\n')
            self.ui.Tabs.setCurrentIndex(2)
        else:
            self.subscriber.client.disconnect()
            self.ui.errorTextBrowser.append('disconnect')
            self.connect_mqtt = False
            self.ui.MQTTStat.setPixmap(self.err)
            self.ui.MQTTStat.setToolTip("MQTT not connected")

    def run_mqtt(self):
        if self.connect_mqtt:
            self.subscriber.subscribe("state")
            self.subscriber.subscribe("coord")
        else:
            self.ui.errorTextBrowser.append('нет подключения к mqtt брокеру\r\n')
            self.ui.Tabs.setCurrentIndex(2)

    def stop_mqtt(self):
        if self.connect_mqtt:
            self.subscriber.unsubscriber("coord")
        else:
            self.ui.errorTextBrowser.append('нет подключения к mqtt брокеру\r\n')
            self.ui.Tabs.setCurrentIndex(2)

    def check_mqtt(self):
        """
        get update полученных координат с mqtt
        """
        if not self.connect_mqtt:
            self.ui.errorTextBrowser.append('нет подключения к mqtt брокеру\r\n')
            self.ui.Tabs.setCurrentIndex(2)
        else:
            self.msg = self.subscriber.coord
            if self.msg == '':
                self.ui.showMqttLable.setText('None')
            else:
                self.ui.showMqttLable.setText(self.msg)
                self.msg = self.msg.split(' ')
                self.xmm = int(self.msg[0])
                self.ymm = int(self.msg[1])
                self.angel = int(self.msg[2])
                self.write_serialport(
                    [f'g0 x{self.xmm} y{self.ymm} z-10 a{self.angel}'])

    # region Line Edit
    def edit_broker(self):
        """
        метод считывания значений из LineEdit
        и занисывания их в словарь конфигурации
        """
        self.broker_edit = self.ui.brokerLineEdit.text()
        self.config_mqtt["settings"]["broker"] = self.broker_edit

    def edit_topic(self):
        self.topic_edit = self.ui.topicLineEdit.text()
        self.config_mqtt["settings"]["topic"]["coordinate"] = self.topic_edit

    def edit_port(self):
        self.port_edit = self.ui.portLineEdit.text()
        self.config_mqtt["settings"]["port"] = self.port_edit

    def edit_client_id(self):
        self.client_id = self.ui.clientIdLineEdit.text()
        self.config_mqtt["settings"]["client_id"] = self.client_id

    def edit_username(self):
        self.username = self.ui.usernameLineEdit.text()
        self.config_mqtt["settings"]["username"] = self.username

    def edit_password(self):
        self.password = self.ui.passwordLineEdit.text()
        self.config_mqtt["settings"]["password"] = self.password
    # endregion

    def save_mqtt(self):
        """
        метод подключаемый к кнопке для сохранения параметров mqtt
        """
        ReadJson.save_json('././configuration/mqttSettingsCamera.json', self.config_mqtt)
        self.ui.errorTextBrowser.append('successfully save mqtt\r\n')
    # endregion

    # region enabled
    def enabled_console(self, value):
        self.ui.Console.setEnabled(value)
        self.ui.Butt_Send.setEnabled(value)

    def enabled_slider(self, value):
        qgrid = self.ui.findChildren(QGridLayout)[0]
        for i in range(0, qgrid.count()):
            obj = qgrid.takeAt(i)
            if obj is not None:
                obj.widget().setEnabled(value)

    def enabled_mqtt(self, value):
        self.ui.checkPushButton.setEnabled(value)
        self.ui.startPushButton.setEnabled(value)
        self.ui.runButton.setEnabled(value)
        self.ui.stopPushButton.setEnabled(value)
        self.ui.showMqttLable.setEnabled(value)

    # endregion

    def start_mqtt(self):
        pass
