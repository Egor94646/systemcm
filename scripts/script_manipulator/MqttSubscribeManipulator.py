import random
import time
from paho.mqtt import client as mqtt_client
from scripts.scripts_common import ReadJson


class MqttSubscribeManipulator:

    def __init__(self, window):
        super(MqttSubscribeManipulator, self).__init__()

        self.window = window


        self.configMqtt = ReadJson.reading_json('././configuration/mqttSettingsManipulator.json')
        self.configMqtt = self.configMqtt["settings"]

        self.broker = self.configMqtt["broker"]
        self.topic_coord = 'K/RX'
        self.topic_state = self.configMqtt["topic"]["state"]
        self.port = self.configMqtt["port"]

        self.client_id = self.configMqtt["client_id"]
        self.username = self.configMqtt["username"]
        self.password = self.configMqtt["password"]

        self.client = mqtt_client.Client(self.client_id)

        self.myGlobalMessagePayload = ''
        self.coord = ''

        self.connect_mqtt()

        self.state = 'standard'

    def connect_mqtt(self):
        """
        метод подключения к mqtt серверу
        """
        global ui
        ui = self.window

        def on_connect(client, userdata, flags, rc):
            """
            метод события подключения к mqtt брокеру
            :param rc: состояния подключения к mqtt серверу
            """
            if rc == 0:
                ui.errorTextBrowser.append('Connected to MQTT Broker!\r\n')
                return 0
            else:
                ui.errorTextBrowser.append("Failed to connect, return code %d \r\n", rc)
                return rc

        self.client.username_pw_set(self.username, self.password)
        self.client.on_connect = on_connect
        self.client.connect(self.broker, self.port)
        self.client.loop_start()

    def publish(self, msg, topic):
        time.sleep(1)
        self.msg = msg
        result = self.client.publish(topic, self.msg)
        status = result[0]

    def subscribe(self, topic):
        """
        метод подписки на топик
        """
        def on_message(client, userdata, msg):
            """
            метод вызываемый при получении сообщения в подключенный топик
            :param msg: полученное сообщения
            """
            print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic\r\n")
            self.window.errorTextBrowser.append(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic\r\n")

            self.coord = msg.payload.decode()

        self.client.subscribe(topic)
        self.client.on_message = on_message

    #def unsubscriber(self, topic):
    #    if topic == "coord":
    #        self.client.unsubscribe(self.topic_coord)
    #    elif topic == "state":
    #        self.client.unsubscribe(self.topic_state)
