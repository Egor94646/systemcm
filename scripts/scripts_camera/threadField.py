import cv2
import sys
from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage, QPixmap
from scripts.scripts_camera import threadDetection as thd


class ThreadField(QThread):
    changePixmap = pyqtSignal(QImage)

    def __init__(self, window, parent=None):
        super().__init__()
        self.window = window
        self.threadactive = True

    def run(self):
        cam = cv2.VideoCapture(0)
        while self.threadactive:
            ret, frame = cam.read()
            dictionary = cv2.aruco.Dictionary_get(cv2.aruco.DICT_6X6_250)
            parameters = cv2.aruco.DetectorParameters_create()
            if ret:
                markerCorners, markerIds, rejectedCandidates = cv2.aruco.detectMarkers(frame,
                                                                                       dictionary,
                                                                                       parameters=parameters)

                frame = cv2.aruco.drawDetectedMarkers(frame, markerCorners, markerIds, borderColor=(0, 0, 250))
                rgbImage = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                h, w, ch = rgbImage.shape
                bytesPerLine = ch * w
                convertToQtFormat = QImage(rgbImage.data, w, h, bytesPerLine, QImage.Format_RGB888)
                self.pix = convertToQtFormat.scaled(540, 400, Qt.KeepAspectRatio)
                self.window.fieldVideo.setPixmap(QPixmap(self.pix))


    def stop(self):
        self.threadactive = False
        self.wait()

    def restart(self):
        self.threadactive = True
        self.start()
