import time

from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QWidget, QLabel, QApplication
from PyQt5 import QtWidgets, uic
import sys
from scripts.scripts_common import ReadJson
from scripts.scripts_camera import threadField
from scripts.scripts_camera import threadObject
from scripts.scripts_camera import objectApp
from scripts.scripts_camera import threadDetection
from scripts.scripts_camera import mqttPublishCamera
import threading
import math


class CameraApp(QWidget):

    def __init__(self):
        super(CameraApp, self).__init__()

        self.camera_ui = uic.loadUi('././ui/Camera_gui.ui')
        self.start = False

        self.configObject = ReadJson.reading_json('././configuration/objectSettings.json')
        self.configField = ReadJson.reading_json('././configuration/fieldSettings.json')
        self.configMqtt = ReadJson.reading_json('././configuration/mqttSettingsCamera.json')

        self.ui_field()
        self.ui_mqtt()
        self.ui_detection()

        self.thObject = objectApp.ThreadField(window=self.camera_ui)
        self.thField = threadField.ThreadField(window=self.camera_ui)
        self.thDetection = threadDetection.ThreadDetection(window=self.camera_ui)
        self.thDetection.max_math()

        self.thField.start()

        self.camera_ui.tabWidget.currentChanged.connect(self.current_tab)

        self.objMqtt = mqttPublishCamera.MqttPublishCamera(self.camera_ui)

        self.x = ''
        self.y = ''
        self.connect = False

    @staticmethod
    def thread_metod(fn):
        def run(*k, **kw):
            t = threading.Thread(target=fn, args=k, kwargs=kw)
            t.start()
            return t

        return run

    def current_tab(self):
        if self.camera_ui.tabWidget.currentIndex() == 0:
            self.thObject.stop()
            self.thDetection.stop()
            self.thField.restart()
        elif self.camera_ui.tabWidget.currentIndex() == 1:
            self.thField.stop()
            self.thDetection.stop()
        elif self.camera_ui.tabWidget.currentIndex() == 3:
            self.thObject.stop()
            self.thField.stop()
        else:
            self.thDetection.stop()
            self.thObject.stop()
            self.thObject.stop()

    # region Field Settings
    # region ui Field
    def ui_field(self):
        self.camera_ui.startPushButton.clicked.connect(self.start_automatic_operation)
        self.show_text_edit_field()
        self.connect_text_edit_field()
        self.camera_ui.savePolePushButton.clicked.connect(self.save_field)
        self.camera_ui.pushButton.clicked.connect(self.publish_mqtt)
        self.camera_ui.detectionPushButton.clicked.connect(self.detection_print)

    def save_field(self):
        ReadJson.save_json("././configuration/fieldSettings.json", self.configField)
        self.camera_ui.textBrowser.append('successfully save field\r\n')

    def show_text_edit_field(self):
        self.camera_ui.aPoleLineEdit.setText(str(self.configField["configuration"]["id_markers"][0][0]))
        self.camera_ui.bPoleLineEdit.setText(str(self.configField["configuration"]["id_markers"][1][0]))
        self.camera_ui.cPoleLineEdit.setText(str(self.configField["configuration"]["id_markers"][2][0]))
        self.camera_ui.dPoleLineEdit.setText(str(self.configField["configuration"]["id_markers"][3][0]))

        self.camera_ui.axCoordLineEdit.setText(str(self.configField["configuration"]["id_markers"][0][1][0]))
        self.camera_ui.bxCoordLineEdit.setText(str(self.configField["configuration"]["id_markers"][1][1][0]))
        self.camera_ui.cxCoordLineEdit.setText(str(self.configField["configuration"]["id_markers"][2][1][0]))
        self.camera_ui.dxCoordLineEdit.setText(str(self.configField["configuration"]["id_markers"][3][1][0]))

        self.camera_ui.ayCoordLineEdit.setText(str(self.configField["configuration"]["id_markers"][0][1][1]))
        self.camera_ui.byCoordLineEdit.setText(str(self.configField["configuration"]["id_markers"][1][1][1]))
        self.camera_ui.cyCoordLineEdit.setText(str(self.configField["configuration"]["id_markers"][2][1][1]))
        self.camera_ui.dyCoordLineEdit.setText(str(self.configField["configuration"]["id_markers"][3][1][1]))

        self.camera_ui.manipulatorLineEdit.setText(str(self.configField["configuration"]["marker_manipulator"]))

    def connect_text_edit_field(self):
        self.camera_ui.aPoleLineEdit.editingFinished.connect(lambda: self.edit_a())
        self.camera_ui.bPoleLineEdit.editingFinished.connect(lambda: self.edit_b())
        self.camera_ui.cPoleLineEdit.editingFinished.connect(lambda: self.edit_c())
        self.camera_ui.dPoleLineEdit.editingFinished.connect(lambda: self.edit_d())

        self.camera_ui.axCoordLineEdit.editingFinished.connect(lambda: self.edit_size_ax())
        self.camera_ui.bxCoordLineEdit.editingFinished.connect(lambda: self.edit_size_bx())
        self.camera_ui.cxCoordLineEdit.editingFinished.connect(lambda: self.edit_size_cx())
        self.camera_ui.dxCoordLineEdit.editingFinished.connect(lambda: self.edit_size_dx())

        self.camera_ui.ayCoordLineEdit.editingFinished.connect(lambda: self.edit_size_ay())
        self.camera_ui.byCoordLineEdit.editingFinished.connect(lambda: self.edit_size_by())
        self.camera_ui.cyCoordLineEdit.editingFinished.connect(lambda: self.edit_size_cy())
        self.camera_ui.dyCoordLineEdit.editingFinished.connect(lambda: self.edit_size_dy())

        self.camera_ui.lineEdit.editingFinished.connect(lambda: self.edit_x())
        self.camera_ui.lineEdit_2.editingFinished.connect(lambda: self.edit_y())

        self.camera_ui.manipulatorLineEdit.editingFinished.connect(lambda: self.edit_manipulator())

    # endregion

    # region Edit
    def edit_a(self):
        self.a = self.camera_ui.aPoleLineEdit.text()
        self.configField["configuration"]["id_markers"][0][0] = int(self.a)

    def edit_b(self):
        self.b = self.camera_ui.bPoleLineEdit.text()
        self.configField["configuration"]["id_markers"][1][0] = int(self.b)

    def edit_c(self):
        self.c = self.camera_ui.cPoleLineEdit.text()
        self.configField["configuration"]["id_markers"][2][0] = int(self.c)

    def edit_d(self):
        self.d = self.camera_ui.dPoleLineEdit.text()
        self.configField["configuration"]["id_markers"][3][0] = int(self.d)

    def edit_size_ax(self):
        self.size_ax = self.camera_ui.axCoordLineEdit.text()
        self.configField["configuration"]["id_markers"][0][1][0] = int(self.size_ax)

    def edit_size_bx(self):
        self.size_bx = self.camera_ui.bxCoordLineEdit.text()
        self.configField["configuration"]["id_markers"][1][1][0] = int(self.size_bx)

    def edit_size_cx(self):
        self.size_cx = self.camera_ui.cxCoordLineEdit.text()
        self.configField["configuration"]["id_markers"][2][1][0] = int(self.size_cx)

    def edit_size_dx(self):
        self.size_dx = self.camera_ui.dxCoordLineEdit.text()
        self.configField["configuration"]["id_markers"][3][1][0] = int(self.size_dx)

    def edit_size_ay(self):
        self.size_ay = self.camera_ui.ayCoordLineEdit.text()
        self.configField["configuration"]["id_markers"][0][1][1] = int(self.size_ay)

    def edit_size_by(self):
        self.size_by = self.camera_ui.byCoordLineEdit.text()
        self.configField["configuration"]["id_markers"][1][1][1] = int(self.size_by)

    def edit_size_cy(self):
        self.size_cy = self.camera_ui.cyCoordLineEdit.text()
        self.configField["configuration"]["id_markers"][2][1][1] = int(self.size_cy)

    def edit_size_dy(self):
        self.size_dy = self.camera_ui.dyCoordLineEdit.text()
        self.configField["configuration"]["id_markers"][3][1][1] = int(self.size_dy)

    def edit_manipulator(self):
        self.manipulator = self.camera_ui.manipulatorLineEdit.text()
        self.configField["configuration"]["marker_manipulator"] = int(self.manipulator)

    # endregion

    # endregion

    # region Mqtt Settings
    # region ui Mqtt
    def ui_mqtt(self):
        self.connect_text_edit_mqtt()
        self.show_text_edit_mqtt()

        self.camera_ui.saveMqttPushButton.clicked.connect(self.save_mqtt)
        self.camera_ui.connectionPushButton.clicked.connect(self.connection_mqtt)
        self.camera_ui.publicMqttPushButton.clicked.connect(self.publish_mqtt)
        self.camera_ui.disconnectPushButton.clicked.connect(self.disconnect_mqtt)

    def connect_text_edit_mqtt(self):
        self.camera_ui.brokerLineEdit.editingFinished.connect(lambda: self.edit_broker())
        self.camera_ui.portLineEdit.editingFinished.connect(lambda: self.edit_port())
        self.camera_ui.topicCoordLineEdit.editingFinished.connect(lambda: self.edit_topic())

        self.camera_ui.clientIdLineEdit.editingFinished.connect(lambda: self.edit_client_id())
        self.camera_ui.usernameLineEdit.editingFinished.connect(lambda: self.edit_usrname())
        self.camera_ui.passwordLineEdit.editingFinished.connect(lambda: self.edit_password())

    def show_text_edit_mqtt(self):
        self.camera_ui.brokerLineEdit.setText(str(self.configMqtt["settings"]["broker"]))
        self.camera_ui.portLineEdit.setText(str(self.configMqtt["settings"]["port"]))
        self.camera_ui.topicCoordLineEdit.setText(str(self.configMqtt["settings"]["topic"]["coordinate_mm"]))
        self.camera_ui.topicStateLineEdit.setText(str(self.configMqtt["settings"]["topic"]["state"]))

        self.camera_ui.clientIdLineEdit.setText(str(self.configMqtt["settings"]["client_id"]))
        self.camera_ui.usernameLineEdit.setText(str(self.configMqtt["settings"]["username"]))
        self.camera_ui.passwordLineEdit.setText(str(self.configMqtt["settings"]["password"]))

    def save_mqtt(self):
        ReadJson.save_json("././configuration/mqttSettingsCamera.json", self.configMqtt)
        self.camera_ui.textBrowser.append('successfully save mqtt\r\n')

    def connection_mqtt(self):
        self.objMqtt.connect_mqtt()
        self.connect = True

    def disconnect_mqtt(self):
        if self.connect:
            self.objMqtt.client.disconnect()
            self.connect = False
        else:
            self.camera_ui.textBrowser.append('дебил, к mqtt подключись\r\n')

    def publish_mqtt(self):
        if self.connect:
            if self.thDetection.x_mm is not None or self.thDetection.y_mm is not None:
                self.objMqtt.publish((str(self.thDetection.x_mm) + ' ' + str(self.thDetection.y_mm) + ' ' + str(
                    self.thDetection.angel_manipulator)), "coordinate_mm")
            else:
                self.objMqtt.publish('не обнаруженны координаты', "coordinate_mm")
        else:
            self.camera_ui.textBrowser.append('дебил, к mqtt подключись\r\n')

    @thread_metod
    def automatic_coord(self, start):
        self.start = start
        while start:
            if self.connect:
                if self.thDetection.x_mm is not None or self.thDetection.y_mm is not None:
                    x = self.thDetection.x_mm
                    y = self.thDetection.y_mm
                    d = (math.sqrt(x ** 2 + y ** 2))
                    PI = math.pi
                    l1 = 150
                    l2 = 240
                    if x >= 0:
                        q1 = math.asin(y / d)
                        q2 = math.acos((d ** 2 + l1 ** 2 - l2 ** 2) / (2 * d * l1))
                        q3 = PI - math.acos((l1 ** 2 + l2 ** 2 - d ** 2) / (2 * l1 * l2))
                        a1 = PI - (q1 + q2)
                        a2 = (q3 + PI / 2)
                        alpha1 = a1 * (180 / PI)
                        alpha2 = a2 * (180 / PI)
                        print(f'{alpha1}, {alpha2}')
                    if x < 0:
                        q1 = math.asin(y / d)
                        q2 = math.acos((d ** 2 + l1 ** 2 - l2 ** 2) / (2 * d * l1))
                        q3 = math.acos((l1 ** 2 + l2 ** 2 - d ** 2) / (2 * l1 * l2))
                        a1 = q1 + q2
                        a2 = PI - ((PI - q3) + PI / 2)
                        alpha1 = a1 * (180 / PI)
                        alpha2 = a2 * (180 / PI)

                    self.objMqtt.publish((str(alpha1) + ' ' + str(alpha2) + ' ' + str(
                        self.thDetection.angel_manipulator)), "coordinate_mm")
                else:
                    self.objMqtt.publish('не обнаруженны координаты', "coordinate_mm")
            else:
                self.camera_ui.textBrowser.append('дебил, к mqtt подключись\r\n')
            time.sleep(5)

    # endregion

    # region Edit mqtt

    def edit_broker(self):
        self.broker = self.camera_ui.brokerLineEdit.text()
        self.configMqtt["settings"]["broker"] = self.broker

    def edit_port(self):
        self.port = self.camera_ui.portLineEdit.text()
        self.configMqtt["settings"]["port"] = self.port

    def edit_topic(self):
        self.topic = self.camera_ui.topicLineEdit.text()
        self.configMqtt["settings"]["port"] = self.topic

    def edit_client_id(self):
        self.client_id = self.camera_ui.clientIdLineEdit.text()
        self.configMqtt["settings"]["client_id"] = self.topic

    def edit_username(self):
        self.username = self.camera_ui.usernameLineEdit.text()
        self.configMqtt["settings"]["username"] = self.username

    def edit_password(self):
        self.password = self.camera_ui.passwordLineEdit.text()
        self.configMqtt["settings"]["password"] = self.password

    def edit_x(self):
        self.x = self.camera_ui.lineEdit.text()

    def edit_y(self):
        self.y = self.camera_ui.lineEdit_2.text()
    # endregion

    # endregion

    # region detection
    def ui_detection(self):
        self.camera_ui.detectionPushButton.clicked.connect(self.detection_print)
        self.camera_ui.startPushButton.clicked.connect(self.start_automatic_operation)
        self.camera_ui.stopPushButton.clicked.connect(self.stop_automatic_operation)

    def detection_print(self):
        x = self.thDetection.x_mm
        y = self.thDetection.y_mm
        self.camera_ui.textBrowser.append('x = ' + str(x) + ' ' + 'y = ' + str(y)+'\r\n')

    def start_automatic_operation(self):
        if self.connect:
            if not self.start:
                self.automatic_coord(True)
                self.objMqtt.publish("automatic", "state")
            else:
                self.camera_ui.textBrowser.append('работа запущена\r\n')
        else:
            self.camera_ui.textBrowser.append('дебил, к mqtt подключись\r\n')

    def stop_automatic_operation(self):
        if self.connect:
            self.objMqtt.publish("standard", "state")
            self.automatic_coord(False)
        else:
            self.camera_ui.textBrowser.append('дебил, к mqtt подключись\r\n')
    # endregion
