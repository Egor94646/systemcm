import cv2
import sys
from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import *
from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage, QPixmap
import numpy as np
from scripts.scripts_common import ReadJson
from PyQt5.QtQml import QQmlApplicationEngine, qmlRegisterType


def set_layout(start_layout, enabled):
    list_layout = [start_layout]
    while list_layout:
        layout = list_layout.pop(0)
        count = layout.count()
        for i in range(0, count):
            w = layout.itemAt(i).widget()
            if w is not None:
                w.setEnabled(enabled)
            else:
                list_layout.append(layout.itemAt(i).layout())


class ThreadField(QThread):
    changePixmap = pyqtSignal(QImage)

    def __init__(self,window, parent=None):
        super().__init__()
        self.window = window
        self.engine = QQmlApplicationEngine()

        self.configObject = ReadJson.reading_json('././configuration/objectSettings.json')

        min = self.configObject["configuration"]["color"]["min"]
        max = self.configObject["configuration"]["color"]["max"]

        self.h_min, self.s_min, self.v_min = min[0], min[1], min[2]
        self.h_max, self.s_max, self.v_max = max[0], max[1], max[2]

        self.ui_object()

        self.threadactive = True

    # region Thread
    def run(self):
        cam = cv2.VideoCapture(0)
        while self.threadactive:
            ret, frame = cam.read()
            if ret:
                self.min = np.array((self.h_min, self.s_min, self.v_min), np.uint8)
                self.max = np.array((self.h_max, self.s_max, self.v_max), np.uint8)
                hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
                frame = cv2.inRange(hsv, self.min, self.max)
                rgbImage = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                h, w, ch = rgbImage.shape
                bytesPerLine = ch * w
                convertToQtFormat = QImage(rgbImage.data, w, h, bytesPerLine, QImage.Format_RGB888)
                pix = convertToQtFormat.scaled(540, 400, Qt.KeepAspectRatio)
                self.window.objectVideo.setPixmap(QPixmap(pix))

    def stop(self):
        self.threadactive = False
        self.wait()

    def restart(self):
        self.threadactive = True
        self.start()
    # endregion

    # region Object Settings

    def ui_object(self):
        set_layout(self.window.objectVerticalLayout, False)
        self.window.saveObjectPushButton.clicked.connect(self.save_object)
        self.window.typeObjectComboBox.activated[str].connect(self.combo_box_type_object)
        self.window.comboBox.activated[str].connect(self.combo_box)
        self.connect_slider()
        self.show_slider_object()
        self.show_line_edit()

        set_layout(self.window.formLayout_2, False)
        set_layout(self.window.horizontalLayout_2, False)

    def combo_box(self, text):
        match text:
            case 'добавить объект':
                set_layout(self.window.objectVerticalLayout, True)
                set_layout(self.window.formLayout_2, False)
                set_layout(self.window.horizontalLayout_2, False)
            case 'выберите объект':
                set_layout(self.window.objectVerticalLayout, False)
            case _:
                self.text = text
                set_layout(self.window.formLayout_2, False)
                set_layout(self.window.horizontalLayout_2, False)

    def combo_box_type_object(self, text):
        match text:
            case 'cube':
                self.configObject["configuration"]["type_object"] = text
                set_layout(self.window.formLayout_2, True)
                set_layout(self.window.horizontalLayout_2, False)
            case 'sphere':
                self.configObject["configuration"]["type_object"] = text
                set_layout(self.window.formLayout_2, False)
                set_layout(self.window.horizontalLayout_2, True)
            case _:
                set_layout(self.window.formLayout_2, False)
                set_layout(self.window.horizontalLayout_2, False)

    def edit_h(self):
        h = int(self.window.lineEdit_6.text())
        self.configObject["configuration"]["size"]["height"] = h

    def edit_w(self):
        w = int(self.window.lineEdit_3.text())
        self.configObject["configuration"]["size"]["width"] = w

    def edit_l(self):
        l = int(self.window.lineEdit_4.text())
        self.configObject["configuration"]["size"]["length"] = l

    def edit_d(self):
        d = int(self.window.lineEdit_5.text())
        self.configObject["configuration"]["size"]["diameter"] = d

    def save_object(self):
        if 'желтая губка' in self.configObject:
            ReadJson.save_json('././configuration/objectSettings.json', self.configObject)
            self.window.errorLabel.setText('successfully save object')


    def show_slider_object(self):
        self.window.hMinSlider.setValue(self.h_min)
        self.window.sMinSlider.setValue(self.s_min)
        self.window.vMinSlider.setValue(self.v_min)

        self.window.hMaxSlider.setValue(self.h_max)
        self.window.sMaxSlider.setValue(self.s_max)
        self.window.vMaxSlider.setValue(self.v_max)

    def connect_slider(self):
        self.window.hMinSlider.valueChanged.connect(lambda: self.h_min_slider())
        self.window.sMinSlider.valueChanged.connect(lambda: self.s_min_slider())
        self.window.vMinSlider.valueChanged.connect(lambda: self.v_min_slider())

        self.window.hMaxSlider.valueChanged.connect(lambda: self.h_max_slider())
        self.window.sMaxSlider.valueChanged.connect(lambda: self.s_max_slider())
        self.window.vMaxSlider.valueChanged.connect(lambda: self.v_max_slider())

        self.window.lineEdit_5.editingFinished.connect(lambda: self.edit_d())

        self.window.lineEdit_3.editingFinished.connect(lambda: self.edit_w())
        self.window.lineEdit_4.editingFinished.connect(lambda: self.edit_l())
        self.window.lineEdit_6.editingFinished.connect(lambda: self.edit_h())

    def show_line_edit(self):
        self.window.lineEdit_3.setText(str(self.configObject["configuration"]["size"]["width"]))
        self.window.lineEdit_4.setText(str(self.configObject["configuration"]["size"]["length"]))
        self.window.lineEdit_6.setText(str(self.configObject["configuration"]["size"]["height"]))

        self.window.lineEdit_5.setText(str(self.configObject["configuration"]["size"]["diameter"]))

        # region Slider

        # region min

    # region Slider
    def h_min_slider(self):
        self.window.hMinLable.setText(str(self.window.hMinSlider.value()))
        self.h_min = self.window.hMinSlider.value()
        self.configObject["configuration"]["color"]["min"][0] = self.h_min

    def s_min_slider(self):
        self.window.sMinLable.setText(str(self.window.sMinSlider.value()))
        self.s_min = self.window.sMinSlider.value()
        self.configObject["configuration"]["color"]["min"][1] = self.s_min

    def v_min_slider(self):
        self.window.vMinLable.setText(str(self.window.vMinSlider.value()))
        self.v_min = self.window.vMinSlider.value()
        self.configObject["configuration"]["color"]["min"][2] = self.v_min

        # endregion

        # region max

    def h_max_slider(self):
        self.window.hMaxLable.setText(str(self.window.hMaxSlider.value()))
        self.h_max = self.window.hMaxSlider.value()
        self.configObject["configuration"]["color"]["max"][0] = self.h_max

    def s_max_slider(self):
        self.window.sMaxLable.setText(str(self.window.sMaxSlider.value()))
        self.s_max = self.window.sMaxSlider.value()
        self.configObject["configuration"]["color"]["max"][1] = self.s_max

    def v_max_slider(self):
        self.window.vMaxLable.setText(str(self.window.vMaxSlider.value()))
        self.v_max = self.window.vMaxSlider.value()
        self.configObject["configuration"]["color"]["max"][2] = self.v_max

    # endregion
    # endregion
