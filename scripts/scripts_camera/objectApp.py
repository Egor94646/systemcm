import time

import cv2
import sys
from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import *
from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage, QPixmap
import numpy as np
from scripts.scripts_common import ReadJson
from PyQt5.QtQml import QQmlApplicationEngine, qmlRegisterType


def set_layout(start_layout, enabled):
    list_layout = [start_layout]
    while list_layout:
        layout = list_layout.pop(0)
        count = layout.count()
        for i in range(0, count):
            w = layout.itemAt(i).widget()
            if w is not None:
                w.setEnabled(enabled)
            else:
                list_layout.append(layout.itemAt(i).layout())


class ThreadField(QThread):
    changePixmap = pyqtSignal(QImage)

    def __init__(self, window, parent=None):
        super().__init__()
        self.window = window
        self.engine = QQmlApplicationEngine()

        self.configObject = ReadJson.reading_json('././configuration/objectSettings.json')

        self.threadactive = False
        set_layout(self.window.objectVerticalLayout, False)
        self.load_ui()

    # region Thread
    def run(self):
        cam = cv2.VideoCapture(0)
        while self.threadactive:
            self.ui_slider()
            ret, frame = cam.read()
            if ret:
                self.min = np.array((self.h_min, self.s_min, self.v_min), np.uint8)
                self.max = np.array((self.h_max, self.s_max, self.v_max), np.uint8)
                hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
                frame = cv2.inRange(hsv, self.min, self.max)
                rgbImage = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                h, w, ch = rgbImage.shape
                bytesPerLine = ch * w
                convertToQtFormat = QImage(rgbImage.data, w, h, bytesPerLine, QImage.Format_RGB888)
                pix = convertToQtFormat.scaled(540, 400, Qt.KeepAspectRatio)
                self.window.objectVideo.setPixmap(QPixmap(pix))

    def stop(self):
        self.threadactive = False
        self.wait()

    def restart(self):
        self.threadactive = True
        self.start()

    # endregion

    def load_ui(self):
        keys = list(self.configObject.keys())
        self.window.comboBox.addItems(keys)
        self.window.comboBox.activated[str].connect(self.combo_box)
        self.window.typeObjectComboBox.activated[str].connect(self.combo_box_type_object)
        self.window.saveObjectPushButton.clicked.connect(self.save_object)

    def combo_box(self, text):
        match text:
            case 'добавить объект':
                self.restart()
                self.save_method = 0
                set_layout(self.window.objectVerticalLayout, True)
                set_layout(self.window.formLayout_2, False)
                set_layout(self.window.horizontalLayout_2, False)
            case 'выберите объект':
                set_layout(self.window.objectVerticalLayout, False)
                self.stop()
            case _:
                self.restart()
                self.save_method = 1
                self.name = text
                set_layout(self.window.objectVerticalLayout, True)
                set_layout(self.window.formLayout_2, False)
                set_layout(self.window.horizontalLayout_2, False)
                self.ui_start()

    def combo_box_type_object(self, text):
        match text:
            case 'cube':
                self.type = text
                set_layout(self.window.formLayout_2, True)
                set_layout(self.window.horizontalLayout_2, False)

            case 'sphere':
                self.type = text
                set_layout(self.window.formLayout_2, False)
                set_layout(self.window.horizontalLayout_2, True)

            case _:
                set_layout(self.window.formLayout_2, False)
                set_layout(self.window.horizontalLayout_2, False)

    def save_object(self):
        if self.type == 'cube':
            self.w = int(self.window.lineEdit_3.text())
            self.h = int(self.window.lineEdit_4.text())
            self.l = int(self.window.lineEdit_6.text())
            self.d = 'None'
        elif self.type == 'sphere':
            self.d = int(self.window.lineEdit_5.text())
            self.w = 'None'
            self.h = 'None'
            self.l = 'None'
        if self.save_method == 0:
            dict = {f"{self.window.nameLineEdit.text()}": {
                "color": {
                    "min": [
                        self.h_min,
                        self.s_min,
                        self.v_min
                    ],
                    "max": [
                        self.h_max,
                        self.s_max,
                        self.v_max
                    ]
                },
                "type_object": self.type,
                "size": {
                    "width": self.w,
                    "height": self.h,
                    "length": self.l,
                    "diameter": self.d
                }
            }}
            self.configObject.update(dict)
            ReadJson.save_json('././configuration/objectSettings.json', self.configObject)
        else:
            self.configObject[self.name]["color"]["min"] = [
                        self.h_min,
                        self.s_min,
                        self.v_min
                    ]
            self.configObject[self.name]["color"]["max"] = [
                        self.h_max,
                        self.s_max,
                        self.v_max
                    ]
            self.configObject[self.name]["type_object"] = self.type
            self.configObject[self.name]["size"]["width"] = self.w
            self.configObject[self.name]["size"]["height"] = self.h
            self.configObject[self.name]["size"]["length"] = self.l
            self.configObject[self.name]["size"]["diameter"] = self.d
            ReadJson.save_json('././configuration/objectSettings.json', self.configObject)

    def ui_slider(self):
        self.h_min = self.window.hMinSlider.value()
        self.s_min = self.window.sMinSlider.value()
        self.v_min = self.window.vMinSlider.value()

        self.h_max = self.window.hMaxSlider.value()
        self.s_max = self.window.sMaxSlider.value()
        self.v_max = self.window.vMaxSlider.value()

        self.window.hMinLable.setText(str(self.window.hMinSlider.value()))
        self.window.sMinLable.setText(str(self.window.sMinSlider.value()))
        self.window.vMinLable.setText(str(self.window.vMinSlider.value()))

        self.window.hMaxLable.setText(str(self.window.hMaxSlider.value()))
        self.window.sMaxLable.setText(str(self.window.sMaxSlider.value()))
        self.window.vMaxLable.setText(str(self.window.vMaxSlider.value()))

    def ui_start(self):
        self.window.nameLineEdit.setText(self.name)
        self.window.hMinLable.setText(str(self.configObject[self.name]["color"]["min"][0]))
        self.window.sMinLable.setText(str(self.configObject[self.name]["color"]["min"][1]))
        self.window.vMinLable.setText(str(self.configObject[self.name]["color"]["min"][2]))

        self.window.hMaxLable.setText(str(self.configObject[self.name]["color"]["max"][0]))
        self.window.sMaxLable.setText(str(self.configObject[self.name]["color"]["max"][1]))
        self.window.vMaxLable.setText(str(self.configObject[self.name]["color"]["max"][2]))

        self.window.hMinSlider.setValue(self.configObject[self.name]["color"]["min"][0])
        self.window.sMinSlider.setValue(self.configObject[self.name]["color"]["min"][1])
        self.window.vMinSlider.setValue(self.configObject[self.name]["color"]["min"][2])

        self.window.hMaxSlider.setValue(self.configObject[self.name]["color"]["max"][0])
        self.window.sMaxSlider.setValue(self.configObject[self.name]["color"]["max"][1])
        self.window.vMaxSlider.setValue(self.configObject[self.name]["color"]["max"][2])

        self.window.lineEdit_3.setText(str(self.configObject[self.name]["size"]["width"]))
        self.window.lineEdit_4.setText(str(self.configObject[self.name]["size"]["length"]))
        self.window.lineEdit_6.setText(str(self.configObject[self.name]["size"]["height"]))

        self.window.lineEdit_5.setText(str(self.configObject[self.name]["size"]["diameter"]))
        t = self.configObject[self.name]["type_object"]
        self.window.typeObjectComboBox.setCurrentText(t)
        self.combo_box_type_object(t)