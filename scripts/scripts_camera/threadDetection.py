import cv2
import sys
from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import QWidget, QLabel, QApplication, QDialog
from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage, QPixmap
import time
from scripts.scripts_common import ReadJson
import numpy as np
from PyQt5 import QtCore, QtWidgets
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from PIL import Image
from scipy import ndimage
import math
from scripts.scripts_camera import mqttPublishCamera


class ThreadDetection(QThread, QWidget):
    changePixmap = pyqtSignal(QImage)
    customer = QtCore.pyqtSignal(str)

    def __init__(self, window):
        super(ThreadDetection, self).__init__()
        self.detection_window = window

        self.threadactive = False
        self.index_min = 0

        self.publishMqtt = mqttPublishCamera.MqttPublishCamera(self.detection_window)

        self.configField = ReadJson.reading_json('././configuration/fieldSettings.json')

        self.id_markers_field = self.configField["configuration"]["id_markers"]
        self.flip_x = self.configField["configuration"]["flip"]["x"]
        self.flip_y = self.configField["configuration"]["flip"]["y"]

        self.configObject = ReadJson.reading_json('././configuration/objectSettings.json')

        self.m = []
        self.pt = []
        self.index0 = None
        self.index1 = None
        self.index2 = None
        self.index3 = None

        self.dictionary_4x4 = cv2.aruco.Dictionary_get(cv2.aruco.DICT_4X4_50)
        self.dictionary = cv2.aruco.Dictionary_get(cv2.aruco.DICT_6X6_250)
        self.parameters = cv2.aruco.DetectorParameters_create()

        self.wmm = None
        self.hmm = None

        self.x_mm = None
        self.y_mm = None

        self.cam = 0

        self.w = []
        self.h = []
        self.order_marks = []

        self.c00 = None
        self.c01 = None
        self.c11 = None
        self.c10 = None

        self.id = []

        self.x_camera = 0
        self.y_camera = 0

        self.x_sm = 0
        self.y_sm = 0

        self.x_manipulator = 0
        self.y_manipulator = 0

        self.coord_center = [0, 0]

        self.coordinate_object = []

        self.reference = 0

        self.angel_manipulator = 0
        self.ui()

    def run(self):
        cam = cv2.VideoCapture(self.cam)
        while self.threadactive:
            self.max_math()
            ret, src = cam.read()
            self.src = src

            self.markerCorners, self.markerIds, self.rejectedCandidates = cv2.aruco.detectMarkers(self.src,
                                                                                                  self.dictionary,
                                                                                               parameters=self.parameters)
            self.src = cv2.blur(self.src, (1, 1))
            self.pt.clear()
            if self.markerIds is not None:
                if len(self.markerIds) >= 4:
                    self.id = []
                    for i in self.markerIds:
                        self.id.append(i[0])
                    result = all(x in self.id for x in self.order_marks)
                    if result:
                        self.pt_angels()
                        self.max_height()
                        self.max_width()
                        self.math_out()

                        self.weight = self.out.shape[1]
                        self.height = self.out.shape[0]

                        moments = cv2.moments(cv2.inRange(cv2.cvtColor(self.out, cv2.COLOR_BGR2HSV), self.hsv_min, self.hsv_max), 1)
                        thresh = cv2.inRange(cv2.cvtColor(self.src, cv2.COLOR_BGR2HSV), self.hsv_min, self.hsv_max)

                        dM01 = moments['m01']
                        dM10 = moments['m10']
                        dArea = moments['m00']

                        if dArea > 0:

                            self.x_camera = int(dM10 / dArea)
                            self.y_camera = int(dM01 / dArea)

                            match self.configObject[self.name]["type_object"]:
                                case 'cube':
                                    self.math_cube(thresh)

                                case 'sphere':
                                    self.math_sphere(thresh)

                        else:
                            self.error(self.time() + " объект не обнаружен")
                        self.out = ndimage.rotate(self.out, 90)
                        rgbImage = cv2.cvtColor(self.out, cv2.COLOR_BGR2RGB)
                        h, w, ch = rgbImage.shape
                        bytesPerLine = ch * w
                        convertToQtFormat = QImage(rgbImage.data, w, h, bytesPerLine, QImage.Format_RGB888)
                        self.pix = convertToQtFormat.scaled(500, 380, Qt.KeepAspectRatio)
                        self.detection_window.detectionVideo.setPixmap(QPixmap(self.pix))
                        self.pt.clear()

                    else:
                        self.error(self.time() + " не найдены заданные маркеры")
                        self.pt.clear()
                else:
                    self.error(self.time() + " Найдено меньше 4х маркеров")
                    self.pt.clear()
            else:
                self.error(self.time() + " Не обнаружены маркеры")
                self.pt.clear()

    def stop(self):
        self.threadactive = False
        self.wait()

    def restart(self):
        self.cam = 0
        self.threadactive = True
        self.start()

    def ui(self):
        keys = list(self.configObject.keys())
        self.detection_window.comboBox_2.addItems(keys)
        self.detection_window.comboBox_2.activated[str].connect(self.combo_box)

    def combo_box(self, text):
        match text:
            case "выбирите объект":
                pass
            case _:
                self.name = text
                color1 = self.configObject[text]["color"]["min"][0]
                color2 = self.configObject[text]["color"]["min"][1]
                color3 = self.configObject[text]["color"]["min"][2]

                color4 = self.configObject[text]["color"]["max"][0]
                color5 = self.configObject[text]["color"]["max"][1]
                color6 = self.configObject[text]["color"]["max"][2]
                self.hsv_min = np.array((color1, color2, color3), np.uint8)
                self.hsv_max = np.array((color4, color5, color6), np.uint8)
                self.restart()

    # region math
    def pt_angels(self):
        for i in self.order_marks:
            center = [int((self.markerCorners[self.id.index(i)][0][0][0] + self.markerCorners[self.id.index(i)][0][2][0]) / 2),
             int((self.markerCorners[self.id.index(i)][0][0][1] + self.markerCorners[self.id.index(i)][0][2][1]) / 2)]
            self.pt.append(center)

    # region max
    def max_width(self):
        width_AD = np.sqrt(((self.pt[0][0] - self.pt[3][0]) ** 2) + ((self.pt[0][1] - self.pt[3][1]) ** 2))
        width_BC = np.sqrt(((self.pt[1][0] - self.pt[2][0]) ** 2) + ((self.pt[1][1] - self.pt[2][1]) ** 2))
        self.MaxWidth = max(int(width_AD), int(width_BC))

    def max_height(self):
        height_AB = np.sqrt(((self.pt[0][0] - self.pt[1][0]) ** 2) + ((self.pt[0][1] - self.pt[1][1]) ** 2))
        height_CD = np.sqrt(((self.pt[2][0] - self.pt[3][0]) ** 2) + ((self.pt[2][1] - self.pt[3][1]) ** 2))
        self.maxHeight = max(int(height_AB), int(height_CD))
    # endregion

    def math_out(self):
        input_pts = np.float32([self.pt[0], self.pt[1], self.pt[2], self.pt[3]])

        output_pts = np.float32([[0, 0],
                                 [0, self.maxHeight - 1],
                                 [self.MaxWidth - 1, self.maxHeight - 1],
                                 [self.MaxWidth - 1, 0]])

        self.M = cv2.getPerspectiveTransform(input_pts, output_pts)
        self.out = cv2.warpPerspective(self.src, self.M, (self.MaxWidth, self.maxHeight), flags=cv2.INTER_LINEAR)


    # endregion

    def error(self, str):
        self.detection_window.textBrowser.append("E: " + str + '\r\n')
        self.publishMqtt.publish("E: " + str + '\r\n', "Error")
        time.sleep(1)

    def max_math(self):
        for i in range(4):
            self.w.append(self.configField["configuration"]["id_markers"][i][1][0])
            self.h.append(self.configField["configuration"]["id_markers"][i][1][1])

        self.maxw = max(self.w)
        self.minw = min(self.w)

        self.maxh = max(self.h)
        self.minh = min(self.h)

        self.wmm = self.maxw - self.minw
        self.hmm = self.maxh - self.minh

        for i in self.configField["configuration"]["id_markers"]:
            if i[1][0] == self.minw and i[1][1] == self.minh:
                self.c00 = i[0]
            elif i[1][0] == self.maxw and i[1][1] == self.minh:
                self.c01 = i[0]
            elif i[1][0] == self.maxw and i[1][1] == self.maxh:
                self.c11 = i[0]
            elif i[1][0] == self.minw and i[1][1] == self.maxh:
                self.c10 = i[0]

        #self.order_marks = [self.c00, self.c01, self.c11, self.c10]
        self.order_marks = [self.c10, self.c11, self.c01, self.c00]

        for i in range(4):
            if self.configField["configuration"]["id_markers"][i][0] == self.order_marks[0]:
                self.index0 = i
            elif self.configField["configuration"]["id_markers"][i][0] == self.order_marks[1]:
                self.index1 = i
            elif self.configField["configuration"]["id_markers"][i][0] == self.order_marks[2]:
                self.index2 = i
            elif self.configField["configuration"]["id_markers"][i][0] == self.order_marks[3]:
                self.index3 = i

        self.detection_window.aLabel.setText(
            str(self.order_marks[0]) + '(' + str(self.configField["configuration"]["id_markers"][
                                                     self.index0][1][0]) + ', ' +
            str(self.configField["configuration"]["id_markers"][
                    self.index0][1][1]) + ')')

        self.detection_window.bLabel.setText(
            str(self.order_marks[1]) + '(' + str(self.configField["configuration"]["id_markers"][
                                                     self.index1][1][0]) + ', ' +
            str(self.configField["configuration"]["id_markers"][
                    self.index1][1][1]) + ')')

        self.detection_window.cLabel.setText(
            str(self.order_marks[2]) + '(' + str(self.configField["configuration"]["id_markers"][
                                                     self.index2][1][0]) + ', ' +
            str(self.configField["configuration"]["id_markers"][
                self.index2][1][1]) + ')')

        self.detection_window.dLabel.setText(
            str(self.order_marks[3]) + '(' + str(self.configField["configuration"]["id_markers"][
                                                     self.index3][1][0]) + ', ' +
            str(self.configField["configuration"]["id_markers"][
                self.index3][1][1]) + ')')

    def max_array(self, arr, max):
        result = []
        for idx, num in enumerate(arr):
            if num == max:
                result.append(idx)
        return result

    def image_rotation(self, img, angel):
        (h, w) = img.shape[:2]
        center = (int(w / 2), int(h / 2))
        rotation_matrix = cv2.getRotationMatrix2D(center, angel, 1)
        rotated = cv2.warpAffine(img, rotation_matrix, (w, h))
        return rotated

    def angel(self, box):
        pass

    def time(self):
        localTime = time.localtime()
        currentTime = time.strftime("%H:%M:%S", localTime)
        return currentTime
    def dist(self, x0, y0, x1, y1):
        dist = np.sqrt((x1-x0)*2 + (y1 - y0)*2)
        return dist

    def math_cube(self, thresh):
        def warPerspective_point(x, y):
            m = [
                [x],
                [y],
                [1]
            ]
            m3 = [[m[0][0] * self.M[0][0] + m[1][0] * self.M[0][1] + m[2][0] * self.M[0][2]],
                  [m[0][0] * self.M[1][0] + m[1][0] * self.M[1][1] + m[2][0] * self.M[1][2]],
                  [m[0][0] * self.M[2][0] + m[1][0] * self.M[2][1] + m[2][0] * self.M[2][2]]]
            return m3

        contours0, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_TREE,
                                                cv2.CHAIN_APPROX_SIMPLE)
        count = 0
        for cnt in contours0:

            rect = cv2.minAreaRect(cnt)
            box = cv2.boxPoints(rect)
            center = [int(rect[0][0]), int(rect[0][1])]

            center_check = warPerspective_point(center[0], center[1])
            center_check = [int(center_check[0][0]), int(center_check[1][0])]
            if center_check[0] < 0 | center_check[1] < 0:
                return
            count += 1
            box = np.int0(box)
            area = int(rect[1][0] * rect[1][1])

            edge1 = np.int0((box[1][0] - box[0][0], box[1][1] - box[0][1]))
            edge2 = np.int0((box[2][0] - box[1][0], box[2][1] - box[1][1]))

            usedEdge = edge1
            if cv2.norm(edge2) > cv2.norm(edge1):
                usedEdge = edge2
            self.reference = (1, 0)
            angle = 180.0 / math.pi * math.acos(
                (self.reference[0] * usedEdge[0] + self.reference[1] * usedEdge[1]) / (
                        cv2.norm(self.reference) * cv2.norm(usedEdge)))

            if area >= 0:

                i = self.configField["configuration"]["marker_manipulator"]
                if i not in self.id:
                    return print("error")

                self.calibration_mm_px = (self.wmm * self.hmm) / (
                        self.out.shape[0] * self.out.shape[1]
                )
                self.x_calibr = self.wmm / self.out.shape[1]
                self.y_calibr = self.hmm / self.out.shape[0]

                self.y_mm = self.pt[2][1] - center[1]
                self.x_mm = self.pt[2][0] - center[0]

                self.k = self.pt[0][0] / self.pt[0][1]
                self.ky = self.pt[0][1] / self.pt[1][1]

                self.x_mm = self.k * self.x_mm
                self.y_mm = self.k * self.y_mm

                center_manipulator = [int((self.markerCorners[self.id.index(i)][0][0][0] +
                               self.markerCorners[self.id.index(i)][0][2][0]) / 2),
                          int((self.markerCorners[self.id.index(i)][0][0][1] +
                               self.markerCorners[self.id.index(i)][0][2][1]) / 2)]

                m_manimpulator = warPerspective_point(center_manipulator[0], center_manipulator[1])
                center_manipulator = [int(m_manimpulator[0][0]), int(m_manimpulator[1][0])]

                m_obj = warPerspective_point(center[0], center[1])
                center = [int(m_obj[0][0]), int(m_obj[1][0])]

                self.x_mm = int((center[0] - center_manipulator[
                    0]) * self.x_calibr)
                self.y_mm = int((center_manipulator[1] - center[
                    1]) * self.y_calibr + 100)
                #cv2.drawContours(self.out, [box], 0, (255, 0, 0), 2)

                #cv2.circle(self.out, (int(m_obj[0][0]), int(m_obj[1][0])), 5, (0, 255, 0), 2)
                #cv2.circle(self.out, (center_manipulator[0], center_manipulator[1]), 5, (255, 255, 0), 2)

                #cv2.putText(self.out, "%d/%d" % (center_manipulator[0], center_manipulator[1]), (center_manipulator[0] + 10, center_manipulator[1] - 10),
                #            cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 0), 2)
                #print(count)

    def math_sphere(self, thresh):
        contours0, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_TREE,
                                                cv2.CHAIN_APPROX_SIMPLE)
        for cnt in contours0:
            if len(cnt) > 10:
                ellipse = cv2.fitEllipse(cnt)
                (x, y), (Ma, ma), angel = ellipse
                x, y = int(x), int(y)
                self.coordinate_object.append([x, y])
                cv2.ellipse(self.out, ellipse, (0, 0, 255), 2)
                cv2.putText(self.out, "%d/%d" % (x, y), (x + 10, y - 10),
                            cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 0), 2)
