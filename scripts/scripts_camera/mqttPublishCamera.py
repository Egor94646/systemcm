import time
from paho.mqtt import client as mqtt_client
from scripts.scripts_common import ReadJson
from PyQt5.QtWidgets import QWidget


class MqttPublishCamera:

    def __init__(self, window):

        self.window = window

        self.configMqtt = ReadJson.reading_json('././configuration/mqttSettingsCamera.json')
        self.configMqtt = self.configMqtt["settings"]

        self.broker = self.configMqtt["broker"]
        self.topics = self.configMqtt["topic"]
        self.port = self.configMqtt["port"]

        self.client_id = self.configMqtt["client_id"]
        self.username = self.configMqtt["username"]
        self.password = self.configMqtt["password"]

        self.client = mqtt_client.Client(self.client_id)

        self.connect = False

    def connect_mqtt(self):

        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                self.window.textBrowser.append('Connected to MQTT Broker!\r\n')
                self.connect = True
                time.sleep(3)

            else:
                self.window.textBrowser.append("Failed to connect, return code %d \r\n", rc)
                self.connect = False
                time.sleep(3)

        self.client.username_pw_set(self.username, self.password)
        self.client.on_connect = on_connect
        self.client.connect(self.broker, self.port)
        self.client.loop_start()

    def publish(self, msg, topic):
        time.sleep(1)
        self.msg = msg
        result = self.client.publish(self.topics[topic], self.msg)
        status = result[0]
        if status == 0:
            self.window.textBrowser.append(f"Send `{self.msg}` to topic `{self.topics[topic]}`\r\n")
        else:
            self.window.textBrowser.append(f"Failed to send message to topic {self.topics[topic]}\r\n")
