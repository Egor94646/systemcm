from work_scripts_camera.Thread import ThreadVideo
from work_scripts_camera.camera import camera
import cv2
import sys
from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import QWidget, QLabel, QApplication, QDialog
from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtSerialPort import QSerialPort, QSerialPortInfo
import serial
from work_scripts_camera import ReadJson


class MainGui(QWidget):
    def __init__(self):
        super().__init__()

        self.Camera = camera()
        self.config_mqtt = ReadJson.reading_mqtt()

        self.ser = serial.Serial('COM3')
        self.ser.baudrate = 9600

        self.ui = uic.loadUi('Gui_1_.ui')

        self.ui.Butt_G28.clicked.connect(self.g28_clicked)
        self.ui.Butt_G30.clicked.connect(self.g30_clicked)

        self.ui.Butt_M1.clicked.connect(self.m1_clicked)
        self.ui.Butt_M2.clicked.connect(self.m2_clicked)
        self.ui.Butt_M3.clicked.connect(self.m3_clicked)
        self.ui.Butt_M4.clicked.connect(self.m4_clicked)

        self.ui.calibrationButton.clicked.connect(self.calibration_clicked)
        self.ui.detectButton.clicked.connect(self.detection_clicked)
        self.ui.runButton.clicked.connect(self.run)

        self.ui.Butt_Close.clicked.connect(self.r)

        self.th = ThreadVideo(mainwindow=self.ui)
        self.th.start()

        self.ui.Mqtt_Setting_Button.clicked.connect(self.settings_mqtt_dialog)


    # region WriteSerial


    def write_serialport(self, str):
        m = str
        m += '\n'
        b = bytes(m, encoding='utf-8')
        self.ser.write(b)

    def g28_clicked(self):
        MainGui.write_serialport(self, 'g28')

    def g30_clicked(self):
        MainGui.write_serialport(self, 'g30')

    def m1_clicked(self):
        MainGui.write_serialport(self, 'm1')

    def m2_clicked(self):
        MainGui.write_serialport(self, 'm2')

    def m3_clicked(self):
        MainGui.write_serialport(self, 'm3')

    def m4_clicked(self):
        MainGui.write_serialport(self, 'm4')

    def run(self):
        MainGui.write_serialport(self, f'G0 X{self.xsm} Y{self.ysm} Z-30 A90')

    def r(self):
        m = self.ser.readline()
        print(m)

    # endregion

    # region camera
    def calibration_clicked(self):
        self.mtx, self.rves, self.tvec = self.Camera.calibration()

    def detection_clicked(self):
        self.xsm, self.ysm = self.Camera.detection()
    # endregion

    # region Settings Mqtt
    def settings_mqtt_dialog(self):
        self.mqtt_window = uic.loadUi('MqttSettings.ui')

        self.mqtt_window.brokerLineEdit.returnPressed.connect(lambda: self.edit_broker())
        self.mqtt_window.portLineEdit.returnPressed.connect(lambda: self.edit_port())
        self.mqtt_window.topicLineEdit.returnPressed.connect(lambda: self.edit_topic())
        self.mqtt_window.clientIdLineEdit.returnPressed.connect(lambda: self.edit_client_id())
        self.mqtt_window.usernameLineEdit.returnPressed.connect(lambda: self.edit_username())
        self.mqtt_window.passwordLineEdit.returnPressed.connect(lambda: self.edit_password())

        self.mqtt_window.applyButton.clicked.connect(self.apply_button)

        self.mqtt_window.show()

    # region Line Edit
    def edit_broker(self):
        self.broker_edit = self.mqtt_window.brokerLineEdit.text()
        self.config_mqtt["settings"]["broker"] = self.broker_edit

    def edit_topic(self):
        self.topic_edit = self.mqtt_window.topicLineEdit.text()
        self.config_mqtt["settings"]["topic"] = self.topic_edit

    def edit_port(self):
        self.port_edit = self.mqtt_window.portLineEdit.text()
        self.config_mqtt["settings"]["port"] = self.port_edit

    def edit_client_id(self):
        self.client_id = self.mqtt_window.clientIdLineEdit.text()
        self.config_mqtt["settings"]["client_id"] = self.client_id

    def edit_username(self):
        self.username = self.mqtt_window.usernameLineEdit.text()
        self.config_mqtt["settings"]["username"] = self.username

    def edit_password(self):
        self.password = self.mqtt_window.passwordLineEdit.text()
        self.config_mqtt["settings"]["password"] = self.password

    # endregion

    def apply_button(self):
        ReadJson.save_json('./configuration/mqttSettingsCamera.json', self.config_mqtt)

    # endregion


def launch():
    app = QtWidgets.QApplication(sys.argv)
    gui = MainGui()
    gui.ui.show()
    app.exec_()
