import cv2
import sys
from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import QWidget, QLabel, QApplication, QDialog
from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage, QPixmap


class ThreadVideo(QThread):
    changePixmap = pyqtSignal(QImage)

    def __init__(self,mainwindow, parent=None):
        super().__init__()
        self.mainwindow = mainwindow

    def run(self):
        cam = cv2.VideoCapture(0)
        while True:
            ret, frame = cam.read()
            dictionary = cv2.aruco.Dictionary_get(cv2.aruco.DICT_6X6_250)
            parameters = cv2.aruco.DetectorParameters_create()
            if ret:
                markerCorners, markerIds, rejectedCandidates = cv2.aruco.detectMarkers(frame,
                                                                                       dictionary,
                                                                                       parameters=parameters)

                frame = cv2.aruco.drawDetectedMarkers(frame, markerCorners, markerIds, borderColor=(0, 0, 250))
                rgbImage = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                h, w, ch = rgbImage.shape
                bytesPerLine = ch * w
                convertToQtFormat = QImage(rgbImage.data, w, h, bytesPerLine, QImage.Format_RGB888)
                pix = convertToQtFormat.scaled(790, 500, Qt.KeepAspectRatio)
                self.mainwindow.sss.setPixmap(QPixmap(pix))


