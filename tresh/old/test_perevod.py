import cv2
import numpy as np

if __name__ == '__main__':
    def nothing(*arg):
        pass

cam = cv2.VideoCapture(0)
crange = [0, 0, 0, 0, 0, 0]
while True:
    ret, src = cam.read()

    hsv = cv2.cvtColor(src, cv2.COLOR_BGR2HSV)

    h_min = np.array((10, 51, 57), np.uint8)
    h_max = np.array((105, 212, 255), np.uint8)

    thresh = cv2.inRange(hsv, h_min, h_max)
    contours0, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_TREE,
                                            cv2.CHAIN_APPROX_SIMPLE)

    for cnt in contours0:
        rect = cv2.minAreaRect(cnt)
        box = cv2.boxPoints(rect)
        center = [int(rect[0][0]), int(rect[0][1])]
        box = np.int0(box)
        area = int(rect[1][0] * rect[1][1])

        edge1 = np.int0((box[1][0] - box[0][0], box[1][1] - box[0][1]))
        edge2 = np.int0((box[2][0] - box[1][0], box[2][1] - box[1][1]))

        usedEdge = edge1
        if cv2.norm(edge2) > cv2.norm(edge1):
            usedEdge = edge2



    cv2.imshow('result', thresh)
    ch = cv2.waitKey(5)
    if ch == 27:
        break

cv2.destroyAllWindows()