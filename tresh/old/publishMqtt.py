import random
import time

from paho.mqtt import client as mqtt_client

broker = 'srv2.clusterfly.ru'
port = 9991
topic = "user_9dcd4eb4/device"
# generate client ID with pub prefix randomly
client_id = 'pub'
username = 'user_9dcd4eb4'
password = 'pass_62b5c0c6'
global ii
ii = 0


def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def publish(client,msg):
    global msg_count
    msg_count = 0

    time.sleep(1)
    result = client.publish(topic, msg)
    # result: [0, 1]
    status = result[0]
    if status == 0:
        print(f"Send `{msg}` to topic `{topic}`")
    else:
        print(f"Failed to send message to topic {topic}")
    msg_count += 1


def run():
    client = connect_mqtt()
    client.loop_start()
    publish(client, 'g0 x-300 y0')

run()