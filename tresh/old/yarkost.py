import cv2
import numpy as np

alpha = 0.3
beta = 80
img_path = "7MeansDenoising/1_1.bmp"
cam = cv2.VideoCapture(0)

ret, src = cam.read()
img = src
img2 = src


def nothing(*arg):
    pass

cv2.namedWindow('image')
cv2.createTrackbar('Alpha', 'image', 0, 300, nothing)
cv2.createTrackbar('Beta', 'image', 0, 255, nothing)
cv2.setTrackbarPos('Alpha', 'image', 100)
cv2.setTrackbarPos('Beta', 'image', 10)

while (True):
    ret, src = cam.read()
    img = src
    img2 = src

    alpha = cv2.getTrackbarPos('Alpha', 'image')
    alpha = alpha * 0.01
    img = np.uint8(np.clip((alpha * img2 + beta), 0, 255))


    beta = cv2.getTrackbarPos('Beta', 'image')
    img = np.uint8(np.clip((alpha * img2 + beta), 0, 255))


    cv2.imshow('image', img)
    if cv2.waitKey(1) == ord('q'):
        break
cv2.destroyAllWindows()
