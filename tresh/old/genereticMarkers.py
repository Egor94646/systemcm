import cv2
import numpy as np

markerImage = np.zeros((200, 200), dtype=np.uint8)

dictionary = cv2.aruco.Dictionary_get(cv2.aruco.DICT_6X6_250)
parameters = cv2.aruco.DetectorParameters_create()

markerImage = cv2.aruco.drawMarker(dictionary, 25, 200, markerImage, 1)
cv2.imwrite("../photo_version_2/marker_4.png", markerImage)