from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot


class ThreadQueue(QThread):

    def __init__(self, ser, window, parent=None):
        super().__init__()
        self.window = window
        self.ser = ser
        self.queue = []
        self.threadactive = True
        self.text = None

    def run(self):
        self.write_serialport(self.queue[0])

        self.text = (self.ser.readline().decode())
        self.window.serialLabel.setText(self.text)
        print(self.text)

        while self.text != 'OK!\r\n':
            self.text = (self.ser.readline().decode())
            self.window.serialLabel.setText(self.text)
            print(self.text)

        del self.queue[0]
        while self.queue is not []:
            self.text = (self.ser.readline().decode())
            self.window.serialLabel.setText(self.text)
            print(self.text)
            self.write_serialport(self.queue[0])
            del self.queue[0]
            if self.text == 'OK!\r\n':
                self.write_serialport(self.queue[0])
                del self.queue[0]
            elif self.text[0] == 'E':
                self.stop()
        self.stop()

    def write_serialport(self, str):
        """
        отправляет в Serial порт манипулятора строку str
        :param str: отправляемая строка
        """
        m = str
        m += '\n'
        b = bytes(m, encoding='utf-8')
        self.ser.write(b)

    def add_queue(self, array):
        self.queue = array

    def stop(self):
        self.threadactive = True
        self.wait()

    def restart(self):
        self.threadactive = True
        self.start()
