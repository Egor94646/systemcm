
from time import sleep
from multiprocessing import Process, Pipe




class first:

    def __init__(self):
        self.var = 1

        pass

    def input_data(self, cat):
        self.var += cat
        return self.var


class second:

    def __init__(self):
        self.var = 1

        pass

    def input_data(self, cat):
        self.var += cat
        return self.var


class third:
    def __init__(self):
        self.var = 1

        pass

    def input_data(self, cat):
        self.var += cat
        return self.var


def firstWorker(connection):
    start, end = Pipe()
    first_class = first()
    connection.send(end)
    while True:
        if connection.poll():
            cat = connection.recv()
            vat = first_class.input_data(cat)
            start.send(vat)
            sleep(1)
            break


def secondWorker(connection):
    start, end = Pipe()
    second_class = second()
    connection.send(end)
    connection = connection.recv()
    while True:
        if connection.poll():
            cat = connection.recv()
            vat = second_class.input_data(cat)
            start.send(vat)
            sleep(1)
            break


def thirdWorker(connection):
    start, end = Pipe()
    third_class = second()
    connection.send(end)
    connection = connection.recv()
    while True:
        if connection.poll():
            cat = connection.recv()
            vat = third_class.input_data(cat)
            start.send(vat)
            sleep(1)
            break

if __name__ == '__main__':
    start1, end1 = Pipe()
    p1 = Process(target=firstWorker, daemon=True, args=(end1, ))
    p1.start()
    connectFirst = start1.recv()

    start2, end2 = Pipe()
    p2 = Process(target=secondWorker, daemon=True, args=(end2,))
    p2.start()
    connectSecond = start2.recv()
    start2.send(connectFirst)

    start3, end3 = Pipe()
    p3 = Process(target=thirdWorker, daemon=True, args=(end3,))
    p3.start()
    connectThird = start3.recv()
    start3.send(connectSecond)

    start1.send(2)
    answer = connectThird.recv()
    print(answer)