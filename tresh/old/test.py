index_number = []
input_array = True
input_index = True


def creat_2d_array(x,y):
    a1 = []
    for j in range(x):
        a2 = []
        for i in range(y):
            a2.append(0)
        a1.append(a2)
    return a1


start_array = creat_2d_array(4, 9)
out_array = creat_2d_array(4, 9)


def drow(array):
    for i in range(4):
        for ii in range(9):
            if array[i][ii] == 1:
                array[i][ii] = "*"
            else:
                array[i][ii] = "-"
    return array


def printArray(array):
   for row in array:
      for x in row:
          print("{:4d}".format(x), end="")
      print()

for i in range(4):
    while start_array[i].count(1) != i * 2 + 1:
        n = int(input(f"введите номер {start_array[i].count(1) + 1} * на {i + 1} строке: ")) - 1
        if n < 8:
            start_array[i][n] = 1
        else:
           print("вы ввели слишком большое число")

for i in range(4):
    for ii in range(i * 2 + 1):
        out_array[i][(4 - i) + ii] = 1

print("input: ")
printArray(start_array)
print("----------------")
printArray(out_array)
