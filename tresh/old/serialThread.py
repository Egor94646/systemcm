import cv2
import sys
from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage, QPixmap


class ThreadSerial(QThread):
    changePixmap = pyqtSignal(QImage)

    def __init__(self, ser, window, parent=None):
        super().__init__()
        self.window = window
        self.ser = ser
        self.threadactive = True
        self.text = None

    def run(self):
        while self.threadactive:
            self.text = (self.ser.readline().decode())
            self.window.serialLabel.setText(self.text)


    def stop(self):
        self.threadactive = False
        self.wait()

    def restart(self):
        self.threadactive = True
        self.start()
