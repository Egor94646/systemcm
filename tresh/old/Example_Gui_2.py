from work_scripts_camera.Thread import ThreadVideo
from work_scripts_camera.camera import camera
import cv2
import sys
from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import QWidget, QLabel, QApplication, QDialog
from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtSerialPort import QSerialPort, QSerialPortInfo
import serial
from work_scripts_camera import ReadJson


class MainGui(QWidget):
    def __init__(self):
        super().__init__()

        self.config_mqtt = ReadJson.reading_mqtt()

        #self.ser = serial.Serial('COM3')
        #self.ser.baudrate = 9600
        self.ui = uic.loadUi('Gui_1_.ui')
        self.ui.Select_Manip.currentTextChanged.connect(self.text_changed)


        self.ui.Butt_G28.clicked.connect(self.g28_clicked)
        self.ui.Butt_G30.clicked.connect(self.g30_clicked)

        self.ui.Butt_M1.clicked.connect(self.m1_clicked)
        self.ui.Butt_M2.clicked.connect(self.m2_clicked)
        self.ui.Butt_M3.clicked.connect(self.m3_clicked)
        self.ui.Butt_M4.clicked.connect(self.m4_clicked)

        #region sliders _init_
        self.ui.Slider_label_1.setText('Vector: x')
        self.ui.Slider_1.valueChanged.connect(self.sl1_value_changed)
        self.ui.Console_Slide_1.editingFinished.connect(self.sl1_edfinish)


        self.ui.Slider_label_2.setText('Vector: y')
        self.ui.Slider_2.valueChanged.connect(self.sl2_value_changed)
        self.ui.Console_slide_2.editingFinished.connect(self.sl2_edfinish)

        self.ui.Slider_label_3.setText('Vector: z')
        self.ui.Slider_3.valueChanged.connect(self.sl3_value_changed)
        self.ui.Console_slide_3.editingFinished.connect(self.sl3_edfinish)

        self.ui.Slider_label_4.setText('Vector: a')
        self.ui.Slider_4.valueChanged.connect(self.sl4_value_changed)
        self.ui.Console_slide_4.editingFinished.connect(self.sl4_edfinish)

        self.ui.Slider_label_5.setText('Sector: ?')
        self.ui.Slider_5.valueChanged.connect(self.sl5_value_changed)
        self.ui.Console_slide_5.editingFinished.connect(self.sl5_edfinish)

        # endregion
        self.ui.calibrationButton.clicked.connect(self.calibration_clicked)
        self.ui.detectButton.clicked.connect(self.detection_clicked)
        self.ui.runButton.clicked.connect(self.run)

        self.th = ThreadVideo(mainwindow=self.ui)
        self.th.start()

        self.ui.Mqtt_Setting_Button.clicked.connect(self.settings_mqtt_dialog)


    # region WriteSerial

    def text_changed(self, text):
        if text == 'Scara':
            self.ui.Slider_1.setMinimum(-380)
            self.ui.Slider_1.setMaximum(380)
            self.ui.Slider_2.setMaximum(240)
            self.ui.Slider_2.setMinimum(-240)
            self.ui.Slider_3.setMinimum(-100)
            self.ui.Slider_4.setMaximum(180)
            self.ui.Slider_5.setMaximum(100)

            self.ui.Butt_M1.setEnabled(False)
            self.ui.Butt_M2.setEnabled(False)
            self.ui.Slider_label_5.setEnabled(False)
            self.ui.Slider_5.setEnabled(False)
            self.ui.Slide_5_label.setEnabled(False)
            self.ui.Console_slide_5.setEnabled(False)


    def write_serialport(self, str):
        m = str
        m += '\n'
        b = bytes(m, encoding='utf-8')
        self.ser.write(b)

    def g28_clicked(self):
        self.write_serialport('g28')

    def g30_clicked(self):
        self.write_serialport('g30')

    def m1_clicked(self):
        self.write_serialport('m1')

    def m2_clicked(self):
        self.write_serialport('m2')


    def m3_clicked(self):
        self.write_serialport('m3')


    def m4_clicked(self):
        self.write_serialport('m4')

    # region sliders
    def sl1_value_changed(self, value):
        self.ui.Console_Slide_1.setText(str(value))
        self.ui.Slide_1_label.setText(str(value))

    def sl1_edfinish(self):
        self.ui.Slider_1.setValue(int(self.ui.Console_Slide_1.text()))
        self.ui.Slide_1_label.setText(str(self.ui.Slider_1.value()))
        self.ui.Console_Slide_1.setText(str(self.ui.Slider_1.value()))

    def sl2_value_changed(self, value):
        self.ui.Console_slide_2.setText(str(value))
        self.ui.Slide_2_label.setText(str(value))

    def sl2_edfinish(self):
        self.ui.Slider_2.setValue(int(self.ui.Console_slide_2.text()))
        self.ui.Slide_2_label.setText(str(self.ui.Slider_2.value()))
        self.ui.Console_slide_2.setText(str(self.ui.Slider_2.value()))

    def sl3_value_changed(self, value):
        self.ui.Console_slide_3.setText(str(value))
        self.ui.Slide_3_label.setText(str(value))

    def sl3_edfinish(self):
        self.ui.Slider_3.setValue(int(self.ui.Console_slide_3.text()))
        self.ui.Slide_3_label.setText(str(self.ui.Slider_3.value()))
        self.ui.Console_slide_3.setText(str(self.ui.Slider_3.value()))

    def sl4_value_changed(self, value):
        self.ui.Console_slide_4.setText(str(value))
        self.ui.Slide_4_label.setText(str(value))

    def sl4_edfinish(self):
        self.ui.Slider_4.setValue(int(self.ui.Console_slide_4.text()))
        self.ui.Slide_4_label.setText(str(self.ui.Slider_4.value()))
        self.ui.Console_slide_4.setText(str(self.ui.Slider_4.value()))

    def sl5_value_changed(self, value):
        self.ui.Console_slide_5.setText(str(value))
        self.ui.Slide_5_label.setText(str(value))

    def sl5_edfinish(self):
        self.ui.Slider_5.setValue(int(self.ui.Console_slide_5.text()))
        self.ui.Slide_5_label.setText(str(self.ui.Slider_5.value()))
        self.ui.Console_slide_5.setText(str(self.ui.Slider_5.value()))

    # endregion

    def run(self):
        print(f'g0 x{self.ui.Slider_1.value()} y{self.ui.Slider_2.value()} z{self.ui.Slider_3.value()} a{self.ui.Slider_4.value()}')
        self.write_serialport(f'G0 X{self.ui.Slider_1.value()} Y{self.ui.Slider_2.value()} Z{self.ui.Slider_3.value()} A{self.ui.Slider_4.value()}')
    # endregion

    # region camera
    def calibration_clicked(self):
        self.mtx, self.rves, self.tvec = camera.camera.calibration()

    def detection_clicked(self):
        self.xsm, self.ysm = camera.camera.detection()
    # endregion

    # region Settings Mqtt
    def settings_mqtt_dialog(self):
        self.mqtt_window = uic.loadUi('MqttSettings.ui')

        self.mqtt_window.brokerLineEdit.returnPressed.connect(lambda: self.edit_broker())
        self.mqtt_window.portLineEdit.returnPressed.connect(lambda: self.edit_port())
        self.mqtt_window.topicLineEdit.returnPressed.connect(lambda: self.edit_topic())
        self.mqtt_window.clientIdLineEdit.returnPressed.connect(lambda: self.edit_client_id())
        self.mqtt_window.usernameLineEdit.returnPressed.connect(lambda: self.edit_username())
        self.mqtt_window.passwordLineEdit.returnPressed.connect(lambda: self.edit_password())

        self.mqtt_window.applyButton.clicked.connect(self.apply_button)

        self.mqtt_window.show()

    # region Line Edit
    def edit_broker(self):
        self.broker_edit = self.mqtt_window.brokerLineEdit.text()
        self.config_mqtt["settings"]["broker"] = self.broker_edit

    def edit_topic(self):
        self.topic_edit = self.mqtt_window.topicLineEdit.text()
        self.config_mqtt["settings"]["topic"] = self.topic_edit

    def edit_port(self):
        self.port_edit = self.mqtt_window.portLineEdit.text()
        self.config_mqtt["settings"]["port"] = self.port_edit

    def edit_client_id(self):
        self.client_id = self.mqtt_window.clientIdLineEdit.text()
        self.config_mqtt["settings"]["client_id"] = self.client_id

    def edit_username(self):
        self.username = self.mqtt_window.usernameLineEdit.text()
        self.config_mqtt["settings"]["username"] = self.username

    def edit_password(self):
        self.password = self.mqtt_window.passwordLineEdit.text()
        self.config_mqtt["settings"]["password"] = self.password

    # endregion

    def apply_button(self):
        ReadJson.save_json('./configuration/mqttSettingsCamera.json', self.config_mqtt)

    # endregion


def launch():
    app = QtWidgets.QApplication(sys.argv)
    gui = MainGui()
    gui.ui.show()
    app.exec_()
launch()