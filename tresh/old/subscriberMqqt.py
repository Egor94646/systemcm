import random

from paho.mqtt import client as mqtt_client


broker = 'srv2.clusterfly.ru'
port = 9991
topic = "user_9dcd4eb4/coordinates"
# generate client ID with pub prefix randomly
client_id = 'sub'
username = 'user_9dcd4eb4'
password = 'pass_62b5c0c6'


def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")
    client.subscribe(topic)
    client.on_message = on_message


def run():
    client = connect_mqtt()
    subscribe(client)
    client.loop_forever()
run()



