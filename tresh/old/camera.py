import cv2
import numpy as np
import os
import glob
import time
import matplotlib.pyplot as plt
from PIL import Image
from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QWidget, QLabel, QApplication


class camera(QWidget):
    def __init__(self):
        super(camera, self).__init__()

        self.images = glob.glob('*.png')
        self.pattern_size = (4, 7)
        self.criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

        self.objp = np.zeros((1, self.pattern_size[0] * self.pattern_size[1], 3), np.float32)
        self.objp[0, :, :2] = np.mgrid[0:self.pattern_size[0], 0:self.pattern_size[1]].T.reshape(-1, 2)
        self.prev_img_shape = None

        self.objpoints = []
        self.imgpoints = []

        self.dictionary = cv2.aruco.Dictionary_get(cv2.aruco.DICT_6X6_250)
        self.parameters = cv2.aruco.DetectorParameters_create()

        self.wsm = 50
        self.hsm = 50

        self.hsv_min = np.array((0, 206, 86), np.uint8)
        self.hsv_max = np.array((181, 243, 127), np.uint8)

    # region Working with the manipulator

    def calibration(self):
        """
        метод калибровки камеры
        :return: матрицу калибровки, вектор поворота, вектор перемещения
        """
        for fname in self.images:
            img = cv2.imread(fname)
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            self.retval, self.corners = cv2.findChessboardCorners(img, self.pattern_size,
                    cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_FAST_CHECK + cv2.CALIB_CB_NORMALIZE_IMAGE)

            if self.retval == True:
                self.objpoints.append(self.objp)
                self.corners2 = cv2.cornerSubPix(gray, self.corners, (11, 11), (-1, -1), self.criteria)

            h, w = img.shape[:2]
            ret, self.mtx, self.dist, rvecs, tvecs = cv2.calibrateCamera(self.objpoints, self.imgpoints,
                                                               gray.shape[::-1], None, None)
        return self.mtx, rvecs, tvecs

    def detection(self):
        """
        обнаружение крассного объекта
        :return:x координату в см и у координату в см
        """
        cam = cv2.VideoCapture(0)
        ret, self.src = cam.read()

        self.markerCorners, self.markerIds, self.rejectedCandidates = cv2.aruco.detectMarkers(self.src,
                                                                self.dictionary, parameters=self.parameters)

        if len(self.markerIds) == 4:
            m = [self.markerIds[0][0], self.markerIds[1][0], self.markerIds[2][0], self.markerIds[3][0]]

            pt_a = [int((self.markerCorners[m.index(33)][0][0][0] + self.markerCorners[m.index(33)][0][2][0]) / 2),
                    int((self.markerCorners[m.index(33)][0][0][1] + self.markerCorners[m.index(33)][0][2][1]) / 2)]

            pt_b = [int((self.markerCorners[m.index(4)][0][0][0] + self.markerCorners[m.index(4)][0][2][0]) / 2),
                    int((self.markerCorners[m.index(4)][0][0][1] + self.markerCorners[m.index(4)][0][2][1]) / 2)]

            pt_c = [int((self.markerCorners[m.index(2)][0][0][0] + self.markerCorners[m.index(2)][0][2][0]) / 2),
                    int((self.markerCorners[m.index(2)][0][0][1] + self.markerCorners[m.index(2)][0][2][1]) / 2)]

            pt_d = [int((self.markerCorners[m.index(1)][0][0][0] + self.markerCorners[m.index(1)][0][2][0]) / 2),
                    int((self.markerCorners[m.index(1)][0][0][1] + self.markerCorners[m.index(1)][0][2][1]) / 2)]

            width_AD = np.sqrt(((pt_a[0] - pt_d[0]) ** 2) + ((pt_a[1] - pt_d[1]) ** 2))
            width_BC = np.sqrt(((pt_b[0] - pt_c[0]) ** 2) + ((pt_b[1] - pt_c[1]) ** 2))
            MaxWidth = max(int(width_AD), int(width_BC))

            height_AB = np.sqrt(((pt_a[0] - pt_b[0]) ** 2) + ((pt_a[1] - pt_b[1]) ** 2))
            height_CD = np.sqrt(((pt_c[0] - pt_b[0]) ** 2) + ((pt_c[1] - pt_d[1]) ** 2))
            maxHeight = max(int(height_AB), int(height_CD))

            input_pts = np.float32([pt_a, pt_b, pt_c, pt_d])
            output_pts = np.float32([[0, 0],
                                     [0, maxHeight - 1],
                                     [MaxWidth - 1, maxHeight - 1],
                                     [MaxWidth - 1, 0]])

            M = cv2.getPerspectiveTransform(input_pts, output_pts)
            self.out = cv2.warpPerspective(self.src, M, (MaxWidth, maxHeight), flags=cv2.INTER_LINEAR)

            self.weight = self.out.shape[1]
            self.height = self.out.shape[0]

            hsv = cv2.cvtColor(self.out, cv2.COLOR_BGR2HSV)
            thresh = cv2.inRange(hsv, self.hsv_min, self.hsv_max)

            moments = cv2.moments(thresh, 1)
            dM01 = moments['m01']
            dM10 = moments['m10']
            dArea = moments['m00']

          #  self.out =  cv2.circle(self.out, (1, 1), 2, (250,0,0), -1)
           # cv2.imshow('out', self.out)
            #cv2.waitKey(0)

            if dArea > 50:
                x = int(dM10 / dArea)
                y = int(dM01 / dArea)

                x_sm = int((self.wsm / self.weight) * x)
                y_sm = int((self.hsm / self.height) * y)

                return x_sm * -10, y_sm * 10 
            else:
                return None, None

        else:
            return None, None
    # endregion



