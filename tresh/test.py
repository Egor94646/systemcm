#!/usr/bin/env python

import sys
import math
import numpy as np
import cv2 as cv
hsv_min = np.array((0, 77, 17), np.uint8)
hsv_max = np.array((208, 255, 255), np.uint8)
ellipse = []
def DetectionClickEvent(event, x, y, flags, param):
    if event == cv.EVENT_LBUTTONDOWN:
        c = []
        for elli in ellipse:
            if x != elli[0][0]:
                if y != elli[0][1]:
                    c.append(math.hypot(abs(x - elli[0][0]), abs(y - elli[0][1])))
        if c[0] - c[1] >= 0:
            print(f'{ellipse[1][0][0]} , {ellipse[1][0][1]}')
        else:
            print(f'{ellipse[0][0][0]} , {ellipse[0][0][1]}')
        print(x, ",", y)

if __name__ == '__main__':
    fn = 'donuts.png'
    img = cv.imread(fn)

    hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
    thresh = cv.inRange(hsv, hsv_min, hsv_max )
    contours0, hierarchy = cv.findContours(thresh.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    i=0
    for cnt in contours0:
        if len(cnt)>4:
            ellipse.append(cv.fitEllipse(cnt))
            cv.ellipse(img,ellipse[i],(0,0,255),2)
            i+=1
    cv.ellipse(img, (256, 256), (100, 50), 0, 0, 360, 255, 2)
    cv.imshow('contours', img)
    cv.setMouseCallback('contours', DetectionClickEvent)
    cv.waitKey()
    cv.destroyAllWindows()