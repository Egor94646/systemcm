from PyQt5 import QtWidgets, uic
from PyQt5.QtSerialPort import QSerialPort, QSerialPortInfo
from PyQt5.QtCore import QIODevice
from threading import Thread, Timer
import time
import math
import cv2
import MQTT_Receiver

x = 0.0
y = 0.0
z = -490.0
app = QtWidgets.QApplication([])
ui = uic.loadUi('my_design.ui')
ui.setWindowTitle('Serial port')

# mqttDaemon = Thread(target=MQTT_Receiver.run, args=())
# mqttDaemon.start()
# tfs = time.time()

serial = QSerialPort()
serial.setBaudRate(9600)
portList = []
ports = QSerialPortInfo().availablePorts()
for port in ports:
    portList.append(port.portName())
print(portList)
ui.List.addItems(portList)


def onOpen():
    serial.setPortName(ui.List.currentText())
    serial.open(QIODevice.ReadWrite)
    print(serial.readAll())


def onClose():
    serial.close()


def onG28():
    m = 'g28'
    m += '\n'
    serial.write(m.encode())


def r():
    m = serial.readLine()
    print(m)


def AngleCounter(X, Y, Z):
    StaticShoulder = 110
    Mobile1st = 110
    Mobile2nd = 480
    Dx = 20
    Dz = 30
    Xc = X + Dx
    Yc = Y
    Zc = Z + Dz
    DyBC = Yc
    PxzM2nd = math.sqrt(Mobile2nd ** 2 - DyBC ** 2)
    ACxz = math.sqrt((Xc - 110) ** 2 + Zc ** 2)
    Angle1 = (ACxz ** 2 + Mobile1st ** 2 - PxzM2nd ** 2) / (2 * ACxz * Mobile1st)
    Angle1 = math.acos(Angle1)
    Angle1 = math.degrees(Angle1)
    CQxy = math.sqrt(Xc ** 2 + Zc ** 2 + StaticShoulder ** 2)
    Angle2 = (ACxz ** 2 + StaticShoulder ** 2 - CQxy ** 2) / (2 * ACxz * StaticShoulder)
    Angle2 = math.acos(Angle2)
    Angle2 = math.degrees(Angle2)
    Angle = Angle1 + Angle2 - 180
    return (Angle)


def CoordinatesCounter(X1, Y1, Z1):
    X2 = X1 * math.cos(math.radians(120)) + Y1 * math.sin(math.radians(120))
    Y2 = -X1 * math.sin(math.radians(120)) + Y1 * math.cos(math.radians(120))
    X3 = X1 * math.cos(math.radians(-120)) + Y1 * math.sin(math.radians(-120))
    Y3 = -X1 * math.sin(math.radians(-120)) + Y1 * math.cos(math.radians(-120))
    return (AngleCounter(X1, Y1, Z1), AngleCounter(X2, Y2, Z1), AngleCounter(X3, Y3, Z1))


def lbl():
    ui.X_label.setText(str(ui.X_slider.value()))
    ui.Y_label.setText(str(ui.Y_slider.value()))
    ui.Z_label.setText(str(ui.Z_slider.value()))


def xcfkb():
    if ui.radioButtonSlider.isChecked():
        X_value = int(ui.lineEdit_X.text())
        ui.X_slider.setValue(X_value)
    ui.lineEdit_X.clear()


def ycfkb():
    if ui.radioButtonSlider.isChecked():
        Y_value = int(ui.lineEdit_Y.text())
        ui.Y_slider.setValue(Y_value)
    ui.lineEdit_Y.clear()


def zcfkb():
    if ui.radioButtonSlider.isChecked():
        Z_value = int(ui.lineEdit_Z.text())
        ui.Z_slider.setValue(Z_value)
    ui.lineEdit_Z.clear()


# необходимо добавить фукцию для входа и выхода из цикла и гашения других радиобаттонов
def SliderCode():
    if ui.radioButtonConsole.isChecked():
        m = str(ui.lineEdit.text())
        if m != '':
            m += '\n'
            serial.write(m.encode())


def running():
    if ui.radioButtonSlider.isChecked():
        print('X=' + str(ui.X_slider.value()), ' Y=' + str(ui.Y_slider.value()), ' Z=' + str(ui.Z_slider.value()))
        (A, B, C) = CoordinatesCounter(ui.X_slider.value(), ui.Y_slider.value(), ui.Z_slider.value())
        msg = 'G0 A' + str(round(A * -1)) + ' B' + str(round(B * -1)) + ' C' + str(round(C * -1))
        print(msg)
        ui.getting_label.setText(str(msg))
        msg += '\n'
        serial.write(msg.encode())


def updMsg():
    if ui.reqBox.isChecked():
        input = MQTT_Receiver.MSG
        ui.lineEdit.setText(str(input))
        readComand()
        t = Timer(1.0, updMsg)
        t.start()
    else:
        t = Timer(1.0, updMsg)
        t.start()


# уважаемые юные пользователи моего кода, я приношу искренние извинения за предоставленные мною неудобства по поводу работы кода
# я его ещё не до конца подогнал под новый интефейс, так что прошу не трогать
# с уважением. автор кода. лол. ;) не бейте меня, у меня лапки

serial.readyRead.connect(r)
ui.OpenB.clicked.connect(onOpen)
ui.CloseB.clicked.connect(onClose)
ui.G28.clicked.connect(onG28)
# ui.radioButtonConsole.clicked.connect(SliderCode)
ui.X_slider.valueChanged.connect(lbl)
ui.Y_slider.valueChanged.connect(lbl)
ui.Z_slider.valueChanged.connect(lbl)
ui.SendTheCommand.clicked.connect(SliderCode)
ui.lineEdit_X.editingFinished.connect(xcfkb)
ui.lineEdit_Y.editingFinished.connect(ycfkb)
ui.lineEdit_Z.editingFinished.connect(zcfkb)
ui.Run.clicked.connect(running)
# updMsg()
ui.show()
app.exec()
